''' Strong Password Detection

Author: <Francisco Martinez>\

P.S. I received alot of help from the internet
'''



import re

capReg = re.compile(r'.*[A-Z].*')
lowerReg = re.compile(r'.*[a-z].*')
digitReg = re.compile(r'.*\d.*')


def strong_password(string):
    '''Given a string, return True if is strong, False otherwise

    A strong password is definied as one that is at least eight
    characters logn, contains both uppercase and lowercase characters,
    and has at least one digit.

    '''
    if capReg.search(string) and lowerReg.search(string) and digitReg.search(string):
        return True
    else:
        return False

pw = 'Doyouactuallylovethenumber21'
print(strong_password(pw))
