''' Regex Version of strip

Author: <Francisco Martinez>

P.S. I received alot of help from the internet
'''






import re

def restrip(string, chars=None):

    '''Given a string, do the same thing as the strip() string method.

    If no other arguments are passed other than the string, then
    whitespace characters will be removed from the beginning and end
    of the string. Otherwise, the characters speci ed in the second
    argu- ment to the function will be removed from the string.

    '''
    chars = r"\A[\s{0}]+|[\s{0}]+\Z".format(re.escape(chars)) if chars else r"\A\s+|\s+\Z"
    return re.sub(chars, '', string)

print(restrip(" ([Please don't strip me!']  )  ", " ()[]'"))
# the second arguement should be filled in with characters you want to get rid of
# \A[\s\ \(\)\[\]\']+|[\s\ \(\)\[\]\']+\Z
# no more stripping
print(restrip(" ([Please don't strip me!']  )  "))
# if no character is selected to be stripped it will not be stripped
# \A\s+|\s+\Z
# ([no more stripping']  )
