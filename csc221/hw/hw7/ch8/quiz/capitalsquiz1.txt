name:

Date:

Period:

                    State Capitals Quiz (Form 1) 

1. What is the captial of Kansas?
 A. Albany
 B. Topeka
 C. Little Rock
 D. Columbus

2. What is the captial of Indiana?
 A. Indianapolis
 B. Austin
 C. Cheyenne
 D. Madison

3. What is the captial of Louisiana?
 A. Boise
 B. Boston
 C. Baton Rouge
 D. Bismarck

4. What is the captial of Maryland?
 A. Sacramento
 B. Juneau
 C. Annapolis
 D. Oklahoma City

5. What is the captial of Minnesota?
 A. Cheyenne
 B. Saint Paul
 C. Trenton
 D. Boston

6. What is the captial of New Hampshire?
 A. Atlanta
 B. Dover
 C. Concord
 D. Santa Fe

7. What is the captial of Montana?
 A. Helena
 B. Springfield
 C. Nashville
 D. Salem

8. What is the captial of Tennessee?
 A. Boston
 B. Juneau
 C. Honolulu
 D. Nashville

9. What is the captial of Hawaii?
 A. Augusta
 B. Lincoln
 C. Hartford
 D. Honolulu

10. What is the captial of Ohio?
 A. Raleigh
 B. Jackson
 C. Madison
 D. Columbus

11. What is the captial of Massachusetts?
 A. Topeka
 B. Boston
 C. Baton Rouge
 D. Honolulu

12. What is the captial of Washington?
 A. Dover
 B. Harrisburg
 C. Baton Rouge
 D. Olympia

13. What is the captial of Connecticut?
 A. Nashville
 B. Hartford
 C. Raleigh
 D. Sacramento

14. What is the captial of West Virginia?
 A. Raleigh
 B. Springfield
 C. Cheyenne
 D. Charleston

15. What is the captial of Rhode Island?
 A. Jackson
 B. Providence
 C. Trenton
 D. Phoenix

16. What is the captial of Colorado?
 A. Frankfort
 B. Santa Fe
 C. Honolulu
 D. Denver

17. What is the captial of Mississippi?
 A. Jackson
 B. Frankfort
 C. Denver
 D. Madison

18. What is the captial of North Dakota?
 A. Carson City
 B. Bismarck
 C. Montpelier
 D. Raleigh

19. What is the captial of Iowa?
 A. Saint Paul
 B. Tallahassee
 C. Des Moines
 D. Atlanta

20. What is the captial of Kentucky?
 A. Frankfort
 B. Austin
 C. Providence
 D. Honolulu

21. What is the captial of Delaware?
 A. Oklahoma City
 B. Dover
 C. Little Rock
 D. Nashville

22. What is the captial of South Carolina?
 A. Boston
 B. Topeka
 C. Columbia
 D. Salem

23. What is the captial of Oklahoma?
 A. Oklahoma City
 B. Raleigh
 C. Santa Fe
 D. Nashville

24. What is the captial of Alaska?
 A. Salt Lake City
 B. Indianapolis
 C. Juneau
 D. Oklahoma City

25. What is the captial of Pennsylvania?
 A. Oklahoma City
 B. Olympia
 C. Columbus
 D. Harrisburg

26. What is the captial of Wisconsin?
 A. Boise
 B. Bismarck
 C. Madison
 D. Nashville

27. What is the captial of California?
 A. Atlanta
 B. Olympia
 C. Tallahassee
 D. Sacramento

28. What is the captial of New Mexico?
 A. Jackson
 B. Frankfort
 C. Denver
 D. Santa Fe

29. What is the captial of Oregon?
 A. Honolulu
 B. Salem
 C. Concord
 D. Columbia

30. What is the captial of Arkansas?
 A. Madison
 B. Honolulu
 C. Little Rock
 D. Frankfort

31. What is the captial of Utah?
 A. Pierre
 B. Salt Lake City
 C. Juneau
 D. Cheyenne

32. What is the captial of Virginia?
 A. Richmond
 B. Providence
 C. Indianapolis
 D. Topeka

33. What is the captial of Idaho?
 A. Providence
 B. Boise
 C. Indianapolis
 D. Oklahoma City

34. What is the captial of Alabama?
 A. Des Moines
 B. Montgomery
 C. Saint Paul
 D. Austin

35. What is the captial of Nebraska?
 A. Madison
 B. Concord
 C. Honolulu
 D. Lincoln

36. What is the captial of Illinois?
 A. Springfield
 B. Trenton
 C. Atlanta
 D. Sacramento

37. What is the captial of Texas?
 A. Tallahassee
 B. Austin
 C. Hartford
 D. Indianapolis

38. What is the captial of South Dakota?
 A. Trenton
 B. Tallahassee
 C. Helena
 D. Pierre

39. What is the captial of Florida?
 A. Honolulu
 B. Juneau
 C. Tallahassee
 D. Carson City

40. What is the captial of New Jersey?
 A. Trenton
 B. Springfield
 C. Columbus
 D. Helena

41. What is the captial of Wyoming?
 A. Montgomery
 B. Trenton
 C. Cheyenne
 D. Little Rock

42. What is the captial of Vermont?
 A. Hartford
 B. Richmond
 C. Honolulu
 D. Montpelier

43. What is the captial of Maine?
 A. Montpelier
 B. Raleigh
 C. Baton Rouge
 D. Augusta

44. What is the captial of Missouri?
 A. Honolulu
 B. Madison
 C. Little Rock
 D. Jefferson City

45. What is the captial of Nevada?
 A. Nashville
 B. Charleston
 C. Carson City
 D. Denver

46. What is the captial of Michigan?
 A. Topeka
 B. Montpelier
 C. Saint Paul
 D. Lansing

47. What is the captial of Arizona?
 A. Saint Paul
 B. Montpelier
 C. Phoenix
 D. Lincoln

48. What is the captial of New York?
 A. Helena
 B. Little Rock
 C. Columbus
 D. Albany

49. What is the captial of Georgia?
 A. Albany
 B. Sacramento
 C. Trenton
 D. Atlanta

50. What is the captial of North Carolina?
 A. Carson City
 B. Charleston
 C. Dover
 D. Raleigh

