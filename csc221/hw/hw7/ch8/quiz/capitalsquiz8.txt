name:

Date:

Period:

                    State Capitals Quiz (Form 8) 

1. What is the captial of Kansas?
 A. Cheyenne
 B. Denver
 C. Topeka
 D. Springfield

2. What is the captial of Minnesota?
 A. Montpelier
 B. Carson City
 C. Richmond
 D. Saint Paul

3. What is the captial of Kentucky?
 A. Pierre
 B. Phoenix
 C. Concord
 D. Frankfort

4. What is the captial of Wisconsin?
 A. Salem
 B. Jefferson City
 C. Madison
 D. Annapolis

5. What is the captial of Colorado?
 A. Richmond
 B. Denver
 C. Jackson
 D. Honolulu

6. What is the captial of Ohio?
 A. Phoenix
 B. Indianapolis
 C. Columbus
 D. Salt Lake City

7. What is the captial of Virginia?
 A. Charleston
 B. Richmond
 C. Topeka
 D. Little Rock

8. What is the captial of Vermont?
 A. Salem
 B. Montpelier
 C. Pierre
 D. Nashville

9. What is the captial of Mississippi?
 A. Little Rock
 B. Bismarck
 C. Jackson
 D. Hartford

10. What is the captial of Delaware?
 A. Boise
 B. Dover
 C. Concord
 D. Richmond

11. What is the captial of Maine?
 A. Helena
 B. Providence
 C. Augusta
 D. Hartford

12. What is the captial of Louisiana?
 A. Jackson
 B. Salem
 C. Baton Rouge
 D. Salt Lake City

13. What is the captial of North Carolina?
 A. Tallahassee
 B. Austin
 C. Raleigh
 D. Helena

14. What is the captial of South Carolina?
 A. Tallahassee
 B. Columbia
 C. Oklahoma City
 D. Juneau

15. What is the captial of Wyoming?
 A. Raleigh
 B. Cheyenne
 C. Pierre
 D. Austin

16. What is the captial of Alabama?
 A. Des Moines
 B. Topeka
 C. Sacramento
 D. Montgomery

17. What is the captial of Pennsylvania?
 A. Dover
 B. Providence
 C. Harrisburg
 D. Jackson

18. What is the captial of New York?
 A. Springfield
 B. Raleigh
 C. Albany
 D. Boston

19. What is the captial of Georgia?
 A. Harrisburg
 B. Atlanta
 C. Saint Paul
 D. Topeka

20. What is the captial of North Dakota?
 A. Annapolis
 B. Baton Rouge
 C. Bismarck
 D. Frankfort

21. What is the captial of Connecticut?
 A. Denver
 B. Augusta
 C. Hartford
 D. Honolulu

22. What is the captial of Missouri?
 A. Dover
 B. Jefferson City
 C. Lansing
 D. Santa Fe

23. What is the captial of Iowa?
 A. Saint Paul
 B. Des Moines
 C. Raleigh
 D. Hartford

24. What is the captial of California?
 A. Sacramento
 B. Tallahassee
 C. Phoenix
 D. Des Moines

25. What is the captial of Oklahoma?
 A. Oklahoma City
 B. Hartford
 C. Bismarck
 D. Raleigh

26. What is the captial of Michigan?
 A. Providence
 B. Sacramento
 C. Lansing
 D. Trenton

27. What is the captial of Indiana?
 A. Indianapolis
 B. Nashville
 C. Columbia
 D. Lansing

28. What is the captial of Tennessee?
 A. Albany
 B. Salem
 C. Oklahoma City
 D. Nashville

29. What is the captial of West Virginia?
 A. Cheyenne
 B. Charleston
 C. Salt Lake City
 D. Saint Paul

30. What is the captial of Nevada?
 A. Salem
 B. Annapolis
 C. Carson City
 D. Santa Fe

31. What is the captial of Massachusetts?
 A. Bismarck
 B. Indianapolis
 C. Boston
 D. Providence

32. What is the captial of Arkansas?
 A. Salt Lake City
 B. Jefferson City
 C. Little Rock
 D. Atlanta

33. What is the captial of Idaho?
 A. Helena
 B. Boise
 C. Phoenix
 D. Lincoln

34. What is the captial of New Jersey?
 A. Lansing
 B. Sacramento
 C. Trenton
 D. Concord

35. What is the captial of Alaska?
 A. Atlanta
 B. Phoenix
 C. Juneau
 D. Jackson

36. What is the captial of Hawaii?
 A. Jackson
 B. Des Moines
 C. Honolulu
 D. Santa Fe

37. What is the captial of Oregon?
 A. Salem
 B. Juneau
 C. Harrisburg
 D. Madison

38. What is the captial of New Mexico?
 A. Richmond
 B. Trenton
 C. Atlanta
 D. Santa Fe

39. What is the captial of Utah?
 A. Montpelier
 B. Trenton
 C. Salt Lake City
 D. Albany

40. What is the captial of Illinois?
 A. Lincoln
 B. Montgomery
 C. Springfield
 D. Saint Paul

41. What is the captial of South Dakota?
 A. Pierre
 B. Austin
 C. Springfield
 D. Montgomery

42. What is the captial of New Hampshire?
 A. Oklahoma City
 B. Concord
 C. Little Rock
 D. Helena

43. What is the captial of Texas?
 A. Sacramento
 B. Austin
 C. Montpelier
 D. Denver

44. What is the captial of Nebraska?
 A. Springfield
 B. Jackson
 C. Lincoln
 D. Dover

45. What is the captial of Washington?
 A. Richmond
 B. Olympia
 C. Tallahassee
 D. Atlanta

46. What is the captial of Arizona?
 A. Oklahoma City
 B. Trenton
 C. Salt Lake City
 D. Phoenix

47. What is the captial of Maryland?
 A. Carson City
 B. Annapolis
 C. Saint Paul
 D. Raleigh

48. What is the captial of Florida?
 A. Austin
 B. Raleigh
 C. Tallahassee
 D. Indianapolis

49. What is the captial of Rhode Island?
 A. Trenton
 B. Hartford
 C. Salt Lake City
 D. Providence

50. What is the captial of Montana?
 A. Helena
 B. Columbus
 C. Harrisburg
 D. Albany

