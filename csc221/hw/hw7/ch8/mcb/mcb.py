import shelve, pyperclip, sys

mcbShelf = shelve.open('mcb')

if len(sys.argv) == 3 and sys.argv[1].lower()== 'save':
    mcbShelf[sys.argv[2]] = pyperclip.paste()
    #shelf takes the third (second index) in the command line and assigns
    #what is on the clipboard to that keyword
elif len(sys.argv) == 2: #if there are only 2 arguments in the command line
    #list keywords and load content
    if sys.argv[1].lower() == 'list':
        pyperclip.copy(str(list(mcbShelf.keys())))
    elif sys.argv[1] in mcbShelf:
        pyperclip.copy(mcbShelf[sys.argv[1]])

mcbShelf.close()
