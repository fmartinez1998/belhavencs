#!/usr/bin/env python3
''' Mulitplication table maker

Author: <your name>
'''
import argparse

def create_multiplication_table(N):

    '''Given a number N, create an NxN multiplication table in an
    Excel spreadsheet. This sheet's filename should be "output.xlsx"

    Args:
      N (int): size of multiplication table

    Returns:
      None

    '''

    # Place your solution in this function

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('N', help='size of multiplication table')

    args = parser.parse_args()

    create_multiplication_table(args.N)

if __name__=='__main__':
    main()
