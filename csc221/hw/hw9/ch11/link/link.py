#!/usr/bin/env python3
''' Link Verification

Author: <your name>
'''
import argparse

def verify_links(url):
    '''Given a URL of a web page, attempt to download every linked
    page on the page. Flage any pages that have a 404 "Not Found"
    status code and print them out as broken links.

    Args:
      url (str): URL to crawl and verify

    Returns:
      None

    '''

    # Place your solution in this function

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help='URL to verify links')

    args = parser.parse_args()

    verify_links(args.url)

if __name__=='__main__':
    main()
