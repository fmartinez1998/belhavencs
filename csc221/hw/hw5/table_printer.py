''' Table Printer practice project

Author: <Francisco Martinez>
'''
tableData = [['apples', 'oranges', 'cherries', 'banana'],
             ['Alice', 'Bob', 'Carol', 'David'],
             ['dogs', 'cats', 'moose', 'goose']]

def printTable(tableData):
    '''Given list of strings, tableData, displays in a
    well-organized table with each column right-justified'''

    # Place your solution in this function
    colWidths = [0] * len(tableData)

    for i in range(len(tableData)):
        for f in range(len(tableData[i])):
            if len(tableData[i][f]) > colWidths[i]:
                colWidths[i] = len(tableData[i][f])

    for x in range(len(tableData[0])):
        for y in range(len(tableData)):
            print(tableData[y][x].rjust(colWidths[y]), end = ' ')
        print('')

printTable(tableData)
