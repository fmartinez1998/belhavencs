''' Test cases for homework 8.

For each problem, you should create unit tests such that they can be
run against any hw8_solution.py file I might provide.

'''
import hw8_solution

'''
--------------------------------------------------------------------
Problem 1

Create a function, is_odd, that takes a number and returns True if
that number is odd.

Function: is_odd

parameters:
- number

returns: boolean
'''

# Problem 1 tests go here
def is_odd(n):
    try:
        n = round(n)
        if n == int(n):
            round(n)
            return n%2 == 1
    except Exception:
        return 'only intergers are accepted'
    


'''
--------------------------------------------------------------------
Problem 2

Create a function, is_even, that takes a number and returns True if
that number is even. 

Function: is_even

parameters:
- number

returns: boolean
'''

# Problem 2 tests go here

def is_even(n):
    try:
        n = round(n)
        if n == int(n):
            return n%2 == 0
    except Exception:
        return 'only intergers are accepted'

'''
--------------------------------------------------------------------
Problem 3

Create a function, is_mult_of_four, that takes a number and returns
True if that number is a multiple of four. 

Function: is_mult_of_four

parameters:
- number

returns: boolean
'''

# Problem 3 tests go here
def is_mult_of_four(n):
    try:
        n = round(n)
        if n == int(n):
            n = round(n)
            return n%4 == 0
    except Exception:
        return 'only intergers are accepted'



'''
--------------------------------------------------------------------
Problem 4

Create a function, is_mult_of_x, that takes a number and a divisor
and returns True if that number is divisible by divisor

Function: is_mult_of_divisor

parameters:
- number
- divisor

returns: boolean
'''

# Problem 4 tests go here
def is_mult_of_x(n, x):
    try:
        n = round(n)
        if n == int(n):
            return n%x == 0
    except Exception:
        return 'only intergers are accepted'


'''
--------------------------------------------------------------------
Problem 5

Given a string s, return a string made of the first 2 and the last 2
chars of the original string, so 'spring' yields 'spng'. However, if
the string length is less than 2, return instead the empty string.

Function: both_ends

parameters:
- s

returns: string
'''

# Problem 5 tests go here

def both_ends(s):
    try:
        if s == str(s):
            if len(s) <= 2:
                return ''
            else:
                return s[0:2] + s[-2:]
        if s == int(s):
           return 'Only Strings'
            
    except Exception:
        return False

