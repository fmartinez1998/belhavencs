import os
from pathlib import Path

class MoveFileCommand:
    def __init__(self, src, dest):
        self.src = src
        self.dest = dest

    def execute(self):
        self.rename(self.src, self.dest)

    def undo(self):
        self.rename(self.dest, self.src)

    def rename(self, src, dest):
        print("renaming {} to {}".format(src, dest))
        os.rename(src, dest)

class CopyFileCommand:
    def __init__(self,src,dest):
        self.src = scr
        self.dest = dest

    def execute(self):
        self.rename(self.src, self.dest)

    def undo(self):
        self.rename(self.dest, self.src)



def test_move_file_comand():
    command_stack = []

    # commands are just pushed into the command stack
    command_stack.append(MoveFileCommand('foo.txt', 'bar.txt'))
    command_stack.append(MoveFileCommand('bar.txt', 'baz.txt'))

    # verify that none of the target files exist
    assert (not os.path.lexists("foo.txt"))
    assert (not os.path.lexists("bar.txt"))
    assert (not os.path.lexists("baz.txt"))
    try:
        with open("foo.txt", "w"):  # Creating the file
            pass

        # they can be executed later on
        for cmd in command_stack:
            cmd.execute()

        assert not os.path.lexists('foo.txt')
        assert not os.path.lexists('bar.txt')
        assert os.path.lexists('baz.txt')

        # and can also be undone at will
        for cmd in reversed(command_stack):
            cmd.undo()

        assert os.path.lexists('foo.txt')
        assert not os.path.lexists('bar.txt')
        assert not os.path.lexists('baz.txt')
    finally:
        os.unlink("foo.txt")

def test_copy_file_comand():
    command_stack = []

    # commands are just pushed into the command stack
    command_stack.append(CopyFileCommand('foo.txt', 'bar.txt'))
    command_stack.append(CopyFileCommand('bar.txt', 'baz.txt'))

    foo, bar, baz = Path('foo.txt'),Path('bar.txt'),Path('baz.txt')

    # verify that none of the target files exist
    assert not foo.exists()
    assert not bar.exists()
    assert not baz.exists()
    try:
        foo.write_text('some text in this file')

        # they can be executed later on
        for cmd in command_stack:
            cmd.execute()

        assert foo.exists()
        assert bar.exists()
        assert baz.exists()

        assert foo.read_text() == bar.read_text() == baz.read_text()

        # and can also be undone at will
        for cmd in reversed(command_stack):
            cmd.undo()

        assert foo.exists()
        assert not bar.exists()
        assert not baz.exists()
    finally:
        foo.unlink()

if __name__ == "__main__":
    test_move_file_comand()
    test_copy_file_comand()
