class ObjectPool:
    def __init__(self, queue, *, auto_get=False):
        self._queue = queue
        self.item = self._queue.get() if auto_get else None

    def __enter__(self):
        if self.item is None:
            self.item = self._queue.get()
        return self.item

    def __exit__(self, Type, value, traceback):
        if self.item is not None:
            self._queue.put(self.item)
            self.item = None

    def __del__(self):
        # run when object is garbage-collected
        if self.item is not None:
            self._queue.put(self.item)
            self.item = None

def test_objectpool():
    import queue

    def test_object(queue):
        pool = ObjectPool(queue, auto_get=True)
        print('Inside func: {}'.format(pool.item))

    sample_queue = queue.Queue()

    sample_queue.put('yam')

    with ObjectPool(sample_queue) as obj:
        print('Inside with: {}'.format(obj))
    # When the context exits, the object is put back into the pool

    print('Outside with: {}'.format(sample_queue.get()))

    sample_queue.put('sam')
    test_object(sample_queue)
    print('Outside func: {}'.format(sample_queue.get()))

    if sample_queue.empty():
        print('Queue empty!')
    else:
        print(sample_queue.get())


if __name__ == '__main__':
    test_objectpool()
