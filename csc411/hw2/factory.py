class GreekGetter(object):
    '''A simple localizer for Greek'''
    def __init__(self):
        self.translate = dict(dog="σκύλος", cat="γάτα")
    def get(self, message):
        '''We'll punt if we don't have a translation'''
        return self.translate.get(message, str(message))

class EnglishGetter(object):
    '''Simple localizer for English: i.e. echoes the message'''
    def get(self, message):
        return str(message)


def get_localizer(language="english"):
    '''The factory method'''
    languages = dict(english=EnglishGetter, greek=GreekGetter)
    return languages[language]()

def test_localizer():
    # Create our localizers
    e, g = get_localizer(language="english"), get_localizer(language="greek")
    # Localize some text
    for message in "dog parrot cat bear".split():
        print(e.get(message), g.get(message))

if __name__ == '__main__':
    test_localizer()
