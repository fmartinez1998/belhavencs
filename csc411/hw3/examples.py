#!/usr/bin/env python
# -*- coding: utf-8 -*-
# __coconut_hash__ = 0xdd6bce98

# Compiled with Coconut version 1.3.1 [Dead Parrot]

'''Examples from Coconut documentation'''

# Coconut Header: -------------------------------------------------------------

from __future__ import print_function, absolute_import, unicode_literals, division
import sys as _coconut_sys, os.path as _coconut_os_path
_coconut_file_path = _coconut_os_path.dirname(_coconut_os_path.abspath(__file__))
_coconut_sys.path.insert(0, _coconut_file_path)
from __coconut__ import _coconut, _coconut_NamedTuple, _coconut_MatchError, _coconut_tail_call, _coconut_tco, _coconut_igetitem, _coconut_base_compose, _coconut_forward_compose, _coconut_back_compose, _coconut_forward_star_compose, _coconut_back_star_compose, _coconut_pipe, _coconut_star_pipe, _coconut_back_pipe, _coconut_back_star_pipe, _coconut_bool_and, _coconut_bool_or, _coconut_none_coalesce, _coconut_minus, _coconut_map, _coconut_partial
from __coconut__ import *
_coconut_sys.path.remove(_coconut_file_path)

# Compiled Coconut: -----------------------------------------------------------


# Start your examples below here


# Coconut

dubsums = map(lambda x, y: 2 * (x + y), range(0, 10), range(10, 20))
(print)((list)(dubsums))


# Coconut

expnums = map(_coconut_partial(pow, {1: 2}, 2), range(5))
(print)((list)(expnums))

#Coconut

def sq(x):
    return x**2
(print)((sq)((_coconut.operator.add)(*(1, 2))))


#Coconut

fog = _coconut_forward_compose(g, f)
f_into_g = _coconut_base_compose(f, (g, False))


#Coconut

@_coconut_tco  # no infinite loop because :: is lazy
def N(n=0):  # no infinite loop because :: is lazy
    return _coconut_tail_call(_coconut.itertools.chain.from_iterable, (f() for f in (lambda: (n,), lambda: N(n + 1))))  # no infinite loop because :: is lazy
(print)((list)(_coconut_igetitem((_coconut.itertools.chain.from_iterable((f() for f in (lambda: range(-10, 0), lambda: N())))), _coconut.slice(5, 15))))

#python can't be done without complicated iterator comprehension in place of the lazy chaining.

#Coconut

(print)(_coconut_igetitem(map(lambda x: x * 2, range(10**100)), -1))

#Python can't be down without complicated iterator slicing function inspection of custom objects.

#Coconut

(lambda _coconut_none_coalesce_item: calculate_default_value() if _coconut_none_coalesce_item is None else _coconut_none_coalesce_item)(could_be_none())
(lambda x: None if x is None else x.attr[index].method())(could_be_none())

#Coconut

class vector2(_coconut_NamedTuple("vector2", [("x", 'int'), ("y", 'int')]), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
    def __new__(_cls, x=0, y=0):
        return _coconut.tuple.__new__(_cls, (x, y))
    def __abs__(self):
        return (self.x**2 + self.y**2)**.5

v = vector2(3, 4)
(print)(v)  # all data types come with a built-in __repr__
(print)((abs)(v))
v.x = 2  # this will fail because data objects are immutable
(print)(vector2())

# Showcases the syntax, features, and immutable nature of data types, as well as the use of default arguments and type annotations.

class Empty(_coconut.collections.namedtuple("Empty", ""), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
class Leaf(_coconut.collections.namedtuple("Leaf", "n"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
class Node(_coconut.collections.namedtuple("Node", "l r"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__

def size(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], Empty)) and (_coconut.len(_coconut_match_to_args[0]) == 0):
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def size(Empty()) = 0'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def size(Empty()) = 0'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return 0

@addpattern(size)
def size(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], Leaf)) and (_coconut.len(_coconut_match_to_args[0]) == 1):
        n = _coconut_match_to_args[0][0]
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def size(Leaf(n)) = 1'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def size(Leaf(n)) = 1'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return 1

@addpattern(size)
def size(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], Node)) and (_coconut.len(_coconut_match_to_args[0]) == 2):
        l = _coconut_match_to_args[0][0]
        r = _coconut_match_to_args[0][1]
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def size(Node(l, r)) = size(l) + size(r)'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def size(Node(l, r)) = size(l) + size(r)'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return size(l) + size(r)

size(Node(Empty(), Leaf(10))) == 1

#Showcases the algebraic nature of data types when combined with pattern-matching.

class vector(_coconut.collections.namedtuple("vector", "pts"), _coconut.object):
    """Immutable arbitrary-length vector."""

    __slots__ = ()
    __ne__ = _coconut.object.__ne__
    def __new__(_cls, *pts):
        return _coconut.tuple.__new__(_cls, pts)
    @_coconut.classmethod
    def _make(cls, iterable, new=_coconut.tuple.__new__, len=None):
        return new(cls, iterable)
    def _asdict(self):
        return _coconut.OrderedDict([("pts", self[:])])
    def __repr__(self):
        return "vector(*pts=%r)" % (self[:],)
    def _replace(_self, **kwds):
        result = self._make(kwds.pop("pts", _self))
        if kwds:
            raise _coconut.ValueError("Got unexpected field names: %r" % kwds.keys())
        return result
    @_coconut.property
    def pts(self):
        return self[:]
    @_coconut_tco
    def __abs__(self):
        return _coconut_tail_call((_coconut_partial(pow, {1: 0.5}, 2)), (sum)(map(_coconut_partial(pow, {1: 2}, 2), self.pts)))

    @_coconut_tco
    def __add__(self, other):
        _coconut_match_check = False
        _coconut_match_to = other
        if _coconut.isinstance(_coconut_match_to, vector):
            other_pts = _coconut_match_to[0:]
            _coconut_match_check = True
        if not _coconut_match_check:
            _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'vector(*other_pts) = other'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to)))
            _coconut_match_err.pattern = 'vector(*other_pts) = other'
            _coconut_match_err.value = _coconut_match_to
            raise _coconut_match_err

        assert len(other_pts) == len(self.pts)
        return _coconut_tail_call((vector), *map(_coconut.operator.add, self.pts, other_pts))

    @_coconut_tco
    def __neg__(self):
        return _coconut_tail_call((vector), *map(_coconut_minus, self.pts))

    def __sub__(self, other):
        return self + -other

#showcases starred data declaration


#Coconut

def factorial(value):
    _coconut_match_check = False
    _coconut_match_to = value
    if _coconut_match_to == 0:
        _coconut_match_check = True
    if _coconut_match_check:
        return 1
    else:  # possible because of Coconut's
        _coconut_match_check = False  # possible because of Coconut's
        _coconut_match_to = value  # possible because of Coconut's
        if _coconut.isinstance(_coconut_match_to, int):  # possible because of Coconut's
            n = _coconut_match_to  # possible because of Coconut's
            _coconut_match_check = True  # possible because of Coconut's
        if _coconut_match_check and not (n > 0):  # possible because of Coconut's
            _coconut_match_check = False  # possible because of Coconut's
        if _coconut_match_check:  # possible because of Coconut's
            return n * factorial(n - 1)  #   enhanced else statements
        else:
            raise TypeError("invalid argument to factorial of: " + repr(value))

(print)((factorial)(3))

#Showcases else statements, which work much like else statements in python: the code under an else statement is only executed if the corresponding match fails

class point(_coconut.collections.namedtuple("point", "x y"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
    @_coconut_tco
    def transform(self, other):
        _coconut_match_check = False
        _coconut_match_to = other
        if (_coconut.isinstance(_coconut_match_to, point)) and (_coconut.len(_coconut_match_to) == 2):
            x = _coconut_match_to[0]
            y = _coconut_match_to[1]
            _coconut_match_check = True
        if _coconut_match_check:
            return _coconut_tail_call(point, self.x + x, self.y + y)
        else:
            raise TypeError("arg to transform must be a point")
    def __eq__(self, other):
        _coconut_match_check = False
        _coconut_match_to = other
        if (_coconut.isinstance(_coconut_match_to, point)) and (_coconut.len(_coconut_match_to) == 2) and (_coconut_match_to[0] == self.x) and (_coconut_match_to[1] == self.y):
            _coconut_match_check = True
        if _coconut_match_check:
            return True
        else:
            return False

(print)((point(3, 4).transform)(point(1, 2)))
(print)(_coconut.operator.eq(point(1, 2), point(1, 2)))

#Showcases matching to data types. Values defined by the user with the data statement can be matched against and their contents accessed by specifically referencing arguments to the data type's constructor.

class Empty(_coconut.collections.namedtuple("Empty", ""), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
class Leaf(_coconut.collections.namedtuple("Leaf", "n"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
class Node(_coconut.collections.namedtuple("Node", "l r"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
Tree = (Empty, Leaf, Node)  # type union

def depth(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], Tree)) and (_coconut.len(_coconut_match_to_args[0]) == 0):
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def depth(Tree()) = 0'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def depth(Tree()) = 0'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return 0

@addpattern(depth)
def depth(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], Tree)) and (_coconut.len(_coconut_match_to_args[0]) == 1):
        n = _coconut_match_to_args[0][0]
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def depth(Tree(n)) = 1'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def depth(Tree(n)) = 1'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return 1

@addpattern(depth)
def depth(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], Tree)) and (_coconut.len(_coconut_match_to_args[0]) == 2):
        l = _coconut_match_to_args[0][0]
        r = _coconut_match_to_args[0][1]
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def depth(Tree(l, r)) = 1 + max([depth(l), depth(r)])'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def depth(Tree(l, r)) = 1 + max([depth(l), depth(r)])'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return 1 + max([depth(l), depth(r)])

(print)((depth)(Empty()))
(print)((depth)(Leaf(5)))
(print)((depth)(Node(Leaf(2), Node(Empty(), Leaf(3)))))

#Showcases how the combination of data types and match statements can be used to powerful effect to replicate the usage of algebraic data types in other functional programming languages.

def duplicate_first(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) <= 1) and (_coconut.sum((_coconut.len(_coconut_match_to_args) > 0, "l" in _coconut_match_to_kwargs)) == 1):
        _coconut_match_temp_0 = _coconut_match_to_args[0] if _coconut.len(_coconut_match_to_args) > 0 else _coconut_match_to_kwargs.pop("l")
        if (_coconut.isinstance(_coconut_match_temp_0, _coconut.abc.Sequence)) and (_coconut.len(_coconut_match_temp_0) >= 1) and (not _coconut_match_to_kwargs):
            l = _coconut_match_temp_0
            xs = _coconut.list(_coconut_match_temp_0[1:])
            x = _coconut_match_temp_0[0]
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def duplicate_first([x] + xs as l) ='" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def duplicate_first([x] + xs as l) ='
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return [x] + l

(print)((duplicate_first)([1, 2, 3]))

#Showcases head-tail splitting, one of the most common uses of pattern-matching, where a + <var> (or :: <var> for any iterable) at the end of a list or tuple literal can be used to much to match the rest of the sequence.

@_coconut_tco
def sieve(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], _coconut.abc.Iterable)):
        tail = _coconut.iter(_coconut_match_to_args[0])
        _coconut_match_temp_0 = _coconut.tuple(_coconut_igetitem(tail, _coconut.slice(None, 1)))
        if (_coconut.len(_coconut_match_temp_0) == 1) and (not _coconut_match_to_kwargs):
            head = _coconut_match_temp_0[0]
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def sieve([head] :: tail) ='" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def sieve([head] :: tail) ='
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return _coconut_tail_call(_coconut.itertools.chain.from_iterable, (f() for f in (lambda: [head], lambda: sieve((n for n in tail if n % head)))))

@addpattern(sieve)
def sieve(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], _coconut.abc.Iterable)):
        _coconut_match_temp_0 = _coconut.tuple(_coconut_match_to_args[0])
        if (_coconut.len(_coconut_match_temp_0) == 0) and (not _coconut_match_to_kwargs):
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def sieve((||)) = []'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def sieve((||)) = []'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return []

#Showcases how to match against iterators, namely that the empty iterator case ((//)) must come last, otherwise that case will exhaust the whole iterator before any other pattern has a chance to match against it.

#Python
#Can't be done without a long series of checks for each match statement.

#Coconut

def classify_sequence(value):
    out = ""  # unlike with normal matches, only one of the patterns
    _coconut_match_to = value  #  will match, and out will only get appended to once
    _coconut_match_check = False  #  will match, and out will only get appended to once
    if (_coconut.isinstance(_coconut_match_to, _coconut.abc.Sequence)) and (_coconut.len(_coconut_match_to) == 0):  #  will match, and out will only get appended to once
        _coconut_match_check = True  #  will match, and out will only get appended to once
    if _coconut_match_check:  #  will match, and out will only get appended to once
        out += "empty"
    if not _coconut_match_check:
        if (_coconut.isinstance(_coconut_match_to, _coconut.abc.Sequence)) and (_coconut.len(_coconut_match_to) == 1):
            _coconut_match_check = True
        if _coconut_match_check:
            out += "singleton"
    if not _coconut_match_check:
        if (_coconut.isinstance(_coconut_match_to, _coconut.abc.Sequence)) and (_coconut.len(_coconut_match_to) == 2) and (_coconut_match_to[0] == _coconut_match_to[1]):
            x = _coconut_match_to[0]
            _coconut_match_check = True
        if _coconut_match_check:
            out += "duplicate pair of " + str(x)
    if not _coconut_match_check:
        if (_coconut.isinstance(_coconut_match_to, _coconut.abc.Sequence)) and (_coconut.len(_coconut_match_to) == 2):
            _coconut_match_check = True
        if _coconut_match_check:
            out += "pair"
    if not _coconut_match_check:
        if _coconut.isinstance(_coconut_match_to, (tuple, list)):
            _coconut_match_check = True
        if _coconut_match_check:
            out += "sequence"
    if not _coconut_match_check:
        raise TypeError()
    return out

(print)((classify_sequence)([]))
(print)((classify_sequence)(()))
(print)((classify_sequence)([1]))
(print)((classify_sequence)((1, 1)))
(print)((classify_sequence)((1, 2)))
(print)((classify_sequence)((1, 1, 1)))

#Python
#Can't be done without a long series of checks for each match statement.

#Coconut

data = 5
print(data)

#Coconut

def _coconut_lambda_1(x):
    y = 1 / x
    return y * (1 - y)
map(_coconut_lambda_1, L)


#Coconut

(consume)((f() for f in (lambda: print("hello,"), lambda: print("world!"))))

#Python
#can't be done without a complicated iterator comprehension in place of the lazy list.

#Coconut

(_coconut.functools.partial(_coconut.operator.getitem, "123"))(1)
((_coconut.functools.partial(_coconut.functools.partial, mod))(5))(3)


#Coconut

@_coconut_tco
def int_map(f,  # type: _coconut.typing.Callable[[int], int]
     xs,  # type: _coconut.typing.Sequence[int]
    ):
# type: (...) -> _coconut.typing.Sequence[int]
    return _coconut_tail_call((list), map(f, xs))


#Coconut

(print)((list)(map(_coconut.operator.add, *(range(0, 5), range(5, 10)))))

#Coconut

empty_frozen_set = _coconut.frozenset()


#Coconut

(print)((abs)(3 + 4j))

#Coconut

# unlike in Python, this function will never hit a maximum recursion depth error

@_coconut_tco
def factorial(n, acc=1):
    def _coconut_mock_func(n, acc=1): return n, acc
    while True:
        _coconut_match_to = n
        _coconut_match_check = False
        if _coconut_match_to == 0:
            _coconut_match_check = True
        if _coconut_match_check:
            return acc
        if not _coconut_match_check:
            if _coconut.isinstance(_coconut_match_to, int):
                _coconut_match_check = True
            if _coconut_match_check and not (n > 0):
                _coconut_match_check = False
            if _coconut_match_check:
                if factorial is _coconut_recursive_func_21:
                    n, acc = _coconut_mock_func(n - 1, acc * n)
                    continue
                else:
                    return _coconut_tail_call(factorial, n - 1, acc * n)

#showcases tail recursion elimination

# unlike in Python, neither of these functions will ever hit a maximum recursion depth error

        return None
_coconut_recursive_func_21 = factorial
def is_even(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut_match_to_args[0] == 0):
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def is_even(0) = True'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def is_even(0) = True'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return True
@addpattern(is_even)
@_coconut_tco
def is_even(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) <= 1) and (_coconut.sum((_coconut.len(_coconut_match_to_args) > 0, "n" in _coconut_match_to_kwargs)) == 1):
        _coconut_match_temp_0 = _coconut_match_to_args[0] if _coconut.len(_coconut_match_to_args) > 0 else _coconut_match_to_kwargs.pop("n")
        if (_coconut.isinstance(_coconut_match_temp_0, int)) and (not _coconut_match_to_kwargs):
            n = _coconut_match_temp_0
            _coconut_match_check = True
    if _coconut_match_check and not (n > 0):
        _coconut_match_check = False
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def is_even(n is int if n > 0) = is_odd(n-1)'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def is_even(n is int if n > 0) = is_odd(n-1)'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return _coconut_tail_call(is_odd, n - 1)

def is_odd(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut_match_to_args[0] == 0):
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def is_odd(0) = False'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def is_odd(0) = False'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return False
@addpattern(is_odd)
@_coconut_tco
def is_odd(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) <= 1) and (_coconut.sum((_coconut.len(_coconut_match_to_args) > 0, "n" in _coconut_match_to_kwargs)) == 1):
        _coconut_match_temp_0 = _coconut_match_to_args[0] if _coconut.len(_coconut_match_to_args) > 0 else _coconut_match_to_kwargs.pop("n")
        if (_coconut.isinstance(_coconut_match_temp_0, int)) and (not _coconut_match_to_kwargs):
            n = _coconut_match_temp_0
            _coconut_match_check = True
    if _coconut_match_check and not (n > 0):
        _coconut_match_check = False
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def is_odd(n is int if n > 0) = is_even(n-1)'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def is_odd(n is int if n > 0) = is_even(n-1)'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return _coconut_tail_call(is_even, n - 1)

#Showcases tail call optimization

#Python
#Can't be done without rewriting the function(s).

#Coconut

def binexp(x):
    return 2**x
(print)((binexp)(5))

#Coconut

def last_two(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], _coconut.abc.Sequence)) and (_coconut.len(_coconut_match_to_args[0]) >= 2):
        a = _coconut_match_to_args[0][-2]
        b = _coconut_match_to_args[0][-1]
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def last_two(_ + [a, b]):'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def last_two(_ + [a, b]):'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return a, b
def xydict_to_xytuple(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    _coconut_sentinel = _coconut.object()
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut.isinstance(_coconut_match_to_args[0], _coconut.abc.Mapping)) and (_coconut.len(_coconut_match_to_args[0]) == 2):
        _coconut_match_temp_0 = _coconut_match_to_args[0].get("x", _coconut_sentinel)
        _coconut_match_temp_1 = _coconut_match_to_args[0].get("y", _coconut_sentinel)
        if (_coconut_match_temp_0 is not _coconut_sentinel) and (_coconut.isinstance(_coconut_match_temp_0, int)) and (_coconut_match_temp_1 is not _coconut_sentinel) and (_coconut.isinstance(_coconut_match_temp_1, int)) and (not _coconut_match_to_kwargs):
            x = _coconut_match_temp_0
            y = _coconut_match_temp_1
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " '\'def xydict_to_xytuple({"x":x is int, "y":y is int}):\'' " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def xydict_to_xytuple({"x":x is int, "y":y is int}):'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return x, y

(print)((last_two)(range(5)))
(print)((xydict_to_xytuple)({"x": 1, "y": 2}))

#python
#Can't be done without a long series of checks at the top of the function.

#Coconut

def mod(a, b):
    return a % b
(print)(((mod)(x, 2)))



#Coconut

def my_method(self):
    ...


#Coconut

MyClass.my_method = my_method
_coconut_match_check = False
_coconut_match_to = [0, 1, 2, 3]
if (_coconut.isinstance(_coconut_match_to, _coconut.abc.Sequence)) and (_coconut.len(_coconut_match_to) >= 2):
    a = _coconut_match_to[-2]
    b = _coconut_match_to[-1]
    _coconut_match_check = True
if not _coconut_match_check:
    _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'_ + [a, b] = [0, 1, 2, 3]'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to)))
    _coconut_match_err.pattern = '_ + [a, b] = [0, 1, 2, 3]'
    _coconut_match_err.value = _coconut_match_to
    raise _coconut_match_err

print(a, b)

#python
#Can't be done without a long series of checks in place of the destructuring assignment statement.

#Coconut

_coconut_decorator_0 = _coconut_forward_compose(_coconut.functools.partial(wrapper2, arg), wrapper1)
@_coconut_decorator_0
def func(x):
    return x**2

#Coconut

if invalid(input_list):
    raise Exception()
else:
    _coconut_match_check = False
    _coconut_match_to = input_list
    if (_coconut.isinstance(_coconut_match_to, _coconut.abc.Sequence)) and (_coconut.len(_coconut_match_to) >= 1):
        tail = _coconut.list(_coconut_match_to[1:])
        head = _coconut_match_to[0]
        _coconut_match_check = True
    if _coconut_match_check:
        print(head, tail)
    else:
        print(input_list)

#Coconut

try:
    unsafe_func(arg)
except (SyntaxError, ValueError) as err:
    handle(err)

#Coconut

class Empty(_coconut.collections.namedtuple("Empty", ""), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
class Leaf(_coconut.collections.namedtuple("Leaf", "item"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
class Node(_coconut.collections.namedtuple("Node", "left right"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__

#Coconut

global state_a, state_b
state_a, state_b = 10, 100

#Coconut

def f(x):
    return (g)(x)

#Coconut

with open('/path/to/some/file/you/want/to/read') as file_1:
    with open('/path/to/some/file/being/written', 'w') as file_2:
        file_2.write(file_1.read())

#Coconut

(print)((len)(map(_coconut.operator.add, range(5), range(6))))
(print)((tuple)((reversed)(filter(lambda x: x < 5, range(10)))))

#Coconut

def factorial(*_coconut_match_to_args, **_coconut_match_to_kwargs):
    _coconut_match_check = False
    if (_coconut.len(_coconut_match_to_args) == 1) and (_coconut_match_to_args[0] == 0):
        if not _coconut_match_to_kwargs:
            _coconut_match_check = True
    if not _coconut_match_check:
        _coconut_match_err = _coconut_MatchError("pattern-matching failed for " "'def factorial(0) = 1'" " in " + _coconut.repr(_coconut.repr(_coconut_match_to_args)))
        _coconut_match_err.pattern = 'def factorial(0) = 1'
        _coconut_match_err.value = _coconut_match_to_args
        raise _coconut_match_err

    return 1

@addpattern(factorial)
def factorial(n):
    return n * factorial(n - 1)

#Coconut

product = _coconut.functools.partial(reduce, _coconut.operator.mul)
(print)((product)(range(1, 10)))

#Coconut

negatives = takewhile(numiter, lambda x: x < 0)

#Coconut

positives = dropwhile(numiter, lambda x: x < 0)

#Coconut

pairs = groupsof(2, range(1, 11))

#Coconut

original, temp = tee(original)
sliced = _coconut_igetitem(temp, _coconut.slice(5, None))

#Coconut

def list_type(xs):
    _coconut_match_to = reiterable(xs)
    _coconut_match_check = False
    if _coconut.isinstance(_coconut_match_to, _coconut.abc.Iterable):
        tail = _coconut.iter(_coconut_match_to)
        _coconut_match_temp_0 = _coconut.tuple(_coconut_igetitem(tail, _coconut.slice(None, 2)))
        if _coconut.len(_coconut_match_temp_0) == 2:
            fst = _coconut_match_temp_0[0]
            snd = _coconut_match_temp_0[1]
            _coconut_match_check = True
    if _coconut_match_check:
        return "at least 2"
    if not _coconut_match_check:
        if _coconut.isinstance(_coconut_match_to, _coconut.abc.Iterable):
            tail = _coconut.iter(_coconut_match_to)
            _coconut_match_temp_0 = _coconut.tuple(_coconut_igetitem(tail, _coconut.slice(None, 1)))
            if _coconut.len(_coconut_match_temp_0) == 1:
                fst = _coconut_match_temp_0[0]
                _coconut_match_check = True
        if _coconut_match_check:
            return "at least 1"
    if not _coconut_match_check:
        if _coconut.isinstance(_coconut_match_to, _coconut.abc.Iterable):
            _coconut_match_temp_0 = _coconut.tuple(_coconut_match_to)
            if _coconut.len(_coconut_match_temp_0) == 0:
                _coconut_match_check = True
        if _coconut_match_check:
            return "empty"

#Coconut

(consume)(map(print, map(lambda x: x**2, range(10))))

#Coconut

(print)(_coconut_igetitem(count(), 10**100))

#Coconut

class Tuple(_coconut.collections.namedtuple("Tuple", "elems"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
    @_coconut_tco
    def __new__(cls, *elems):
        return _coconut_tail_call(makedata, cls, elems)

#Coconut

fmap(lambda x: x + 1, [1, 2, 3]) == [2, 3, 4]

class Nothing(_coconut.collections.namedtuple("Nothing", ""), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__
class Just(_coconut.collections.namedtuple("Just", "n"), _coconut.object):
    __slots__ = ()
    __ne__ = _coconut.object.__ne__

fmap(lambda x: x * 2, Just(3)) == Just(6)
fmap(lambda x: x * 2, Nothing()) == Nothing()

#Coconut

(consume)(starmap(print, map(range, range(1, 5))))

#Coconut

input_data = [3, 4, 6, 2, 1, 9, 0, 7, 5, 8]
running_max = (list)(scan(max, input_data))

#Coconut

@recursive_iterator
@_coconut_tco
def fib():
    return _coconut_tail_call(_coconut.itertools.chain.from_iterable, (f() for f in (lambda: (1, 1), lambda: map(_coconut.operator.add, fib(), _coconut_igetitem(fib(), _coconut.slice(1, None))))))

#Coconut

(print)((list)(parallel_map(_coconut.functools.partial(pow, 2), range(100))))

#Coconut

(print)((list)(concurrent_map(get_data_for_user, get_all_users())))

#Coconut
