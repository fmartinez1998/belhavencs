'''Examples from Coconut documentation'''
# Start your examples below here


# Coconut

dubsums = map((x, y ) -> 2*(x+y), range(0, 10), range(10, 20))
dubsums |> list |> print


# Coconut

expnums = range(5) |> map$(pow$(?, 2))
expnums |> list |> print

#Coconut

def sq(x) = x**2
(1, 2) |*> (+) |> sq |> print


#Coconut

fog = f..g
f_into_g = f ..> g


#Coconut

def N(n=0) = (n,) :: N(n+1)  # no infinite loop because :: is lazy
(range(-10, 0) :: N())$[5:15] |> list |> print

#python can't be done without complicated iterator comprehension in place of the lazy chaining.

#Coconut

map(x -> x*2, range(10**100))$[-1] |> print

#Python can't be down without complicated iterator slicing function inspection of custom objects.

#Coconut

could_be_none() ?? calculate_default_value()
could_be_none()?.attr[index].method()

#Coconut

data vector2(x:int=0, y:int=0):
    def __abs__(self):
        return (self.x**2 + self.y**2)**.5

v = vector2(3, 4)
v |> print  # all data types come with a built-in __repr__
v |> abs |> print
v.x = 2  # this will fail because data objects are immutable
vector2() |> print

# Showcases the syntax, features, and immutable nature of data types, as well as the use of default arguments and type annotations.

data Empty()
data Leaf(n)
data Node(l, r)

def size(Empty()) = 0

@addpattern(size)
def size(Leaf(n)) = 1

@addpattern(size)
def size(Node(l, r)) = size(l) + size(r)

size(Node(Empty(), Leaf(10))) == 1

#Showcases the algebraic nature of data types when combined with pattern-matching.

data vector(*pts):
    """Immutable arbitrary-length vector."""

    def __abs__(self) =
        self.pts |> map$(pow$(?, 2)) |> sum |> pow$(?, 0.5)

    def __add__(self, other) =
        vector(*other_pts) = other
        assert len(other_pts) == len(self.pts)
        map((+), self.pts, other_pts) |*> vector

    def __neg__(self) =
        self.pts |> map$((-)) |*> vector

    def __sub__(self, other) =
        self + -other

#showcases starred data declaration


#Coconut

def factorial(value):
    match 0 in value:
        return 1
    else: match n is int in value if n > 0:  # possible because of Coconut's
        return n * factorial(n-1)            #   enhanced else statements
    else:
        raise TypeError("invalid argument to factorial of: "+repr(value))

3 |> factorial |> print

#Showcases else statements, which work much like else statements in python: the code under an else statement is only executed if the corresponding match fails

data point(x, y):
    def transform(self, other):
        match point(x, y) in other:
            return point(self.x + x, self.y + y)
        else:
            raise TypeError("arg to transform must be a point")
    def __eq__(self, other):
        match point(=self.x, =self.y) in other:
            return True
        else:
            return False

point(1,2) |> point(3,4).transform |> print
point(1,2) |> (==)$(point(1,2)) |> print

#Showcases matching to data types. Values defined by the user with the data statement can be matched against and their contents accessed by specifically referencing arguments to the data type's constructor.

data Empty()
data Leaf(n)
data Node(l, r)
Tree = (Empty, Leaf, Node)  # type union

def depth(Tree()) = 0

@addpattern(depth)
def depth(Tree(n)) = 1

@addpattern(depth)
def depth(Tree(l, r)) = 1 + max([depth(l), depth(r)])

Empty() |> depth |> print
Leaf(5) |> depth |> print
Node(Leaf(2), Node(Empty(), Leaf(3))) |> depth |> print

#Showcases how the combination of data types and match statements can be used to powerful effect to replicate the usage of algebraic data types in other functional programming languages.

def duplicate_first([x] + xs as l) =
    [x] + l

[1,2,3] |> duplicate_first |> print

#Showcases head-tail splitting, one of the most common uses of pattern-matching, where a + <var> (or :: <var> for any iterable) at the end of a list or tuple literal can be used to much to match the rest of the sequence.

def sieve([head] :: tail) =
    [head] :: sieve(n for n in tail if n % head)

@addpattern(sieve)
def sieve((||)) = []

#Showcases how to match against iterators, namely that the empty iterator case ((//)) must come last, otherwise that case will exhaust the whole iterator before any other pattern has a chance to match against it.

#Python
#Can't be done without a long series of checks for each match statement.

#Coconut

def classify_sequence(value):
    out = ""        # unlike with normal matches, only one of the patterns
    case value:     #  will match, and out will only get appended to once
        match ():
            out += "empty"
        match (_,):
            out += "singleton"
        match (x,x):
            out += "duplicate pair of "+str(x)
        match (_,_):
            out += "pair"
        match _ is (tuple, list):
            out += "sequence"
    else:
        raise TypeError()
    return out

[] |> classify_sequence |> print
() |> classify_sequence |> print
[1] |> classify_sequence |> print
(1,1) |> classify_sequence |> print
(1,2) |> classify_sequence |> print
(1,1,1) |> classify_sequence |> print

#Python
#Can't be done without a long series of checks for each match statement.

#Coconut

\data = 5
print(\data)

#Coconut

L |> map$(def (x) -> y = 1/x; y*(1 - y))


#Coconut

(| print("hello,"), print("world!") |) |> consume

#Python
#can't be done without a complicated iterator comprehension in place of the lazy list.

#Coconut

1 |> "123"[]
mod$ <| 5 <| 3


#Coconut

def int_map(
    f: int -> int,
    xs: int[],
) -> int[] =
    xs |> map$(f) |> list


#Coconut

(range(0, 5), range(5, 10)) |*> map$(+) |> list |> print

#Coconut

empty_frozen_set = f{}


#Coconut

3 + 4i |> abs |> print

#Coconut

# unlike in Python, this function will never hit a maximum recursion depth error

def factorial(n, acc=1):
    case n:
        match 0:
            return acc
        match _ is int if n > 0:
            return factorial(n-1, acc*n)

#showcases tail recursion elimination

# unlike in Python, neither of these functions will ever hit a maximum recursion depth error

def is_even(0) = True
@addpattern(is_even)
def is_even(n is int if n > 0) = is_odd(n-1)

def is_odd(0) = False
@addpattern(is_odd)
def is_odd(n is int if n > 0) = is_even(n-1)

#Showcases tail call optimization

#Python
#Can't be done without rewriting the function(s).

#Coconut

def binexp(x) = 2**x
5 |> binexp |> print

#Coconut

def last_two(_ + [a, b]):
    return a, b
def xydict_to_xytuple({"x":x is int, "y":y is int}):
    return x, y

range(5) |> last_two |> print
{"x":1, "y":2} |> xydict_to_xytuple |> print

#python
#Can't be done without a long series of checks at the top of the function.

#Coconut

def a `mod` b = a % b
(x `mod` 2) `print`



#Coconut

def MyClass.my_method(self):
    ...


#Coconut

_ + [a, b] = [0, 1, 2, 3]
print(a, b)

#python
#Can't be done without a long series of checks in place of the destructuring assignment statement.

#Coconut

@ wrapper1 .. wrapper2 $(arg)
def func(x) = x**2

#Coconut

if invalid(input_list):
    raise Exception()
else: match [head] + tail in input_list:
    print(head, tail)
else:
    print(input_list)

#Coconut

try:
    unsafe_func(arg)
except SyntaxError, ValueError as err:
    handle(err)

#Coconut

data Empty
data Leaf(item)
data Node(left, right)

#Coconut

global state_a, state_b = 10, 100

#Coconut

\\cdef f(x):
    return x |> g

#Coconut

with (open('/path/to/some/file/you/want/to/read') as file_1,
      open('/path/to/some/file/being/written', 'w') as file_2):
    file_2.write(file_1.read())

#Coconut

map((+), range(5), range(6)) |> len |> print
range(10) |> filter$((x) -> x < 5) |> reversed |> tuple |> print

#Coconut

def factorial(0) = 1

@addpattern(factorial)
def factorial(n) = n * factorial(n - 1)

#Coconut

product = reduce$(*)
range(1, 10) |> product |> print

#Coconut

negatives = takewhile(numiter, (x) -> x<0)

#Coconut

positives = dropwhile(numiter, (x) -> x<0)

#Coconut

pairs = range(1, 11) |> groupsof$(2)

#Coconut

original, temp = tee(original)
sliced = temp$[5:]

#Coconut

def list_type(xs):
    case reiterable(xs):
        match [fst, snd] :: tail:
            return "at least 2"
        match [fst] :: tail:
            return "at least 1"
        match (| |):
            return "empty"

#Coconut

range(10) |> map$((x) -> x**2) |> map$(print) |> consume

#Coconut

count()$[10**100] |> print

#Coconut

data Tuple(elems):
    def __new__(cls, *elems):
        return elems |> makedata$(cls)

#Coconut

[1, 2, 3] |> fmap$(x -> x+1) == [2, 3, 4]

data Nothing()
data Just(n)

Just(3) |> fmap$(x -> x*2) == Just(6)
Nothing() |> fmap$(x -> x*2) == Nothing()

#Coconut

range(1, 5) |> map$(range) |> starmap$(print) |> consume

#Coconut

input_data = [3, 4, 6, 2, 1, 9, 0, 7, 5, 8]
running_max = input_data |> scan$(max) |> list

#Coconut

@recursive_iterator
def fib() = (1, 1) :: map((+), fib(), fib()$[1:])

#Coconut

parallel_map(pow$(2), range(100)) |> list |> print

#Coconut

concurrent_map(get_data_for_user, get_all_users()) |> list |> print

#Coconut
