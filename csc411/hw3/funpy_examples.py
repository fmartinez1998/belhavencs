#!/usr/bin/env python3
# funpy_examples.py - Funcional Programming in Python Examples


'ENCAPSULATION'

# configure the data to start with
collection = get_initial_state()
state_var = None
for datum in data_set:
    if condition(state_var):
        state_var = calculate_from(datum)
        new = modify(datum, state_var)
        collection.add_to(new)
    else:
        new = modify_differently(datum)
        collection.add_to(new)
# Now actually work with the data
for thing in collection:
    process(thing)

'''
We might simply remove the “how” of the data construction from
the current scope, and tuck it away in a function that we can think
about in isolation.
'''

# tuck away construction of data
def make_collection(data_set):
    collection = get_initial_state()
    state_var = None
    for datum in data_set:
        if condition(state_var):
            state_var = calculate_from(datum, state_var)
            new = modify(datum, state_var)
            collection.add_to(new)
        else:
            new = modify_differently(datum)
            collection.add_to(new)
    return collection
# Now actually work with the data
for thing in make_collection(data_set):
    process(thing)


'COMPREHENSION'

'''
Using comprehensions is often a way both to make code more com‐
pact and to shift our focus from the “how” to the “what.”
'''

collection = list()
for datum in data_set:
    if condition(datum):
        collection.append(datum)
    else:
        new = modify(datum)
        collection.append(new)

'''
Somewhat more compactly we could write this as:
'''

collection = [d if condition(d) else modify(d)
for d in data_set]


'GENERATORS'

'''
Generator comprehensions have the same syntax as list comprehen‐
sions—other than that there are no square brackets around them
—but they are also lazy.
'''

log_lines = (line for line in read_line(huge_log_file)
                  if complex_condition(line))

'''
this generator compre‐
hension also has imperative versions.
Could be simplified too, but the version
shown is meant to illustrate the behind-the-scenes “how” of a for
loop over an iteratable.
'''

def get_log_lines(log_file):
    line = read_line(log_file)
    while True:
        try:
            if complex_condition(line):
                yield line
            line = read_line(log_file)
        except StopIteration:
            raise

log_lines = get_log_lines(huge_log_file)

class GetLogLines(object):
    def __init__(self, log_file):
        self.log_file = log_file
        self.line = None
    def __iter__(self):
        return self
    def __next__(self):
        if self.line is None:
            self.line = read_line(log_file)
        while not complex_condition(self.line):
            self.line = read_line(self.log_file)
        return self.line

log_lines = GetLogLines(huge_log_file)


'DICTS AND SETS'

'''
dictionaries and sets can be created “all at once”
rather than by repeatedly calling .update() or .add() in a loop.
'''

>>> {i:chr(65+i) for i in range(6)}
{0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F'}

>>>{chr(65+i) for i in range(6)}
{'A', 'B', 'C', 'D', 'E', 'F'}


'RECURSION'

def running_sum(numbers, start=0):
    if len(numbers) == 0:
        print()
        return
    total = numbers[0] + start
    print(total, end=" ")
    running_sum(numbers[1:], total)

'''
in some cases, recursive style, even over sequential opera‐
tions, still expresses algorithms more intuitively and in a way that is
easier to reason about. A slightly less trivial example, factorial in
recursive and iterative style.
'''

def factorialR(N):
    "Recursive factorial function"
    assert isinstance(N, int) and N >= 1
    return 1 if N <= 1 else N * factorialR(N-1)

def factorialI(N):
    "Iterative factorial function"
    assert isinstance(N, int) and N >= 1
    product = 1
    while N >= 1:
        product *= N
        N -= 1
    return product

'''
Fastest version mentioned for factorial() in
Python is in a functional programming style, and also expresses the
“what” of the algorithm well once some higher-order functions are
familiar.
'''

from functools import reduce
from operator import mul
def factorialHOF(n):
    return reduce(mul, range(1, n+1), 1)

'''
the quicksort algorithm is very elegantly
expressed without any state variables or loops, but wholly through
recursion.
'''

def quicksort(lst):
    "Quicksort over a list-like sequence"
    if len(lst) == 0:
        return lst
    pivot = lst[0]
    pivots = [x for x in lst if x == pivot]
    small = quicksort([x for x in lst if x < pivot])
    large = quicksort([x for x in lst if x > pivot])
    return small + pivots + large


'ELIMINATING LOOPS'

'''
If we simply call a function inside a for loop, the built-in higher-
order function map() comes to our aid.
'''

for e in it:       # statement-based loop
    func(e)

'''
entirely equivalent to the functional version,
except there is no repeated rebinding of the variable e involved, and
hence no state.
'''

map(func, it) # map()-based "loop"

'''
A similar technique is available for a functional approach to sequen‐
tial program flow. Most imperative programming consists of state‐
ments that amount to “do this, then do that, then do the other
thing.” If those individual actions are wrapped in functions, map()
lets us do just this.
'''

# let f1, f2, f3 (etc) be functions that perform actions
# an execution utility function
do_it = lambda f, *args: f(*args)
# map()-based action sequence
map(do_it, [f1, f2, f3])

'''
We can combine the sequencing of function calls with passing argu‐
ments from iterables.
'''

hello = lambda first, last: print("Hello", first, last)
bye = lambda first, last: print("Bye", first, last)
_ = list(map(do_it, [hello, bye],
                    ['David','Jane'], ['Mertz','Doe']))
#Hello David Mertz
#Bye Jane Doe

'''
This will pass all the arguments to each of the functions
rather than one argument from each list to each function.
'''

do_all_funcs = lambda fns, *args: [
                      list(map(fn, *args)) for fn in fns]
_ = do_all_funcs([hello, bye],
                 ['David','Jane'], ['Mertz','Doe'])
#Hello David Mertz
#Hello Jane Doe
#Bye David Mertz
#Bye Jane Doe

'''
A map() expression with an iterable of functions to execute to com‐
plete the program is possible to do directly using recursion.
'''

# statement-based while loop
while <cond>:
    <pre-suite>
    if <break_condition>:
        break
    else:
        <suite>

# FP-style recursive while loop
def while_block():
    <pre-suite>
    if <break_condition>:
        return 1
    else:
        <suite>
    return 0
while_FP = lambda: (<cond> and while_block()) or while_FP()
while_FP()

'''
One way to add a more useful condition is to let
while_block() return a more interesting value, and compare that
return value for a termination condition.
'''

# imperative version of "echo()"
def echo_IMP():
    while 1:
        x = input("IMP -- ")
        if x == 'quit':
            break
        else:
            print(x)
echo_IMP()

'''
Removal of the while loop for the functional version.
'''

# FP version of "echo()"
def identity_print(x):
    # "identity with side-effect"
    print(x)
    return x
    echo_FP = lambda: identity_print(input("FP -- "))=='quit' or
echo_FP()
echo_FP()


'NAMED FUNCTIONS AN LAMBDAS'

'''
The most obvious ways to create callables in Python are, in definite
order of obviousness, named functions and lambdas. The only in-
principle difference between them is simply whether they have
a .__qualname__ attribute, since both can very well be bound to one
or more names.
'''

def hello1(name):
    print("Hello", name)

hello2 = lambda name: print("Hello", name)

hello1('David')
#Hello David
hello2('David')
#Hello David
hello1.__qualname__
'hello1'

hello3 = hello2

hello3.__qualname__
'<lambda>'

hello3.__qualname__ = 'hello3'

hello3.__qualname__
'hello3'


'CLOSURES AND CALLABLE INSTANCES'

'''
Classes emphasizing mutable or
rebindable state, and closures emphasizing immutability and pure
functions. Example:
'''

# A class that creates callable adder instances
class Adder(object):
    def __init__(self, n):
        self.n = n
    def __call__(self, m):
        return self.n + m
add5_i = Adder(5) # "instance" or "imperative"

# A class that creates closure adder instances
def make_adder(n):
    def adder(m):
        return m + n
    return adder
add5_f = make_adder(5) # "functional"

add5_i(10)
#15
add5_f(10) # only argument affects result
#15
add5_i.n = 10 # state is readily changeable
add5_i(10) # result is dependent on prior flow
#20

'''
There is a little “gotcha” about how Python binds variables in clo‐
sures. If we want
to manufacture several related closures encapsulating different data.
'''

# almost surely not the behavior we intended!
adders = []
for n in range(5):
    adders.append(lambda m: m+n)

[adder(10) for adder in adders]
#[14, 14, 14, 14, 14]

n = 10
[adder(10) for adder in adders]
#[20, 20, 20, 20, 20]

# almost surely the behavior we intended!
adders = []
for n in range(5):
    adders.append(lambda m, n=n: m+n)

[adder(10) for adder in adders]
#[10, 11, 12, 13, 14]

n = 10
[adder(10) for adder in adders]
#[10, 11, 12, 13, 14]

add4 = adders[4]
add4(10, 100)   # Can override the bound value
#110


'ACCESSORS AND OPERATORS'

'''
Accessors, WHEN created with the @property decorator or
otherwise, are technically callables, albeit accessors are callables with
a limited use in that
they take no arguments as getters, and return no value as setters
'''

class Car(object):
    def __init__(self):
        self._speed = 100

    @property
    def speed(self):
        print("Speed is", self._speed)
        return self._speed

    @speed.setter
    def speed(self, value):
        print("Setting to", value)
        self._speed = value

#>> car = Car()
#>>> car.speed = 80
#Setting to 80
#>>> x = car.speed
#Speed is 80

'''
In an accessor, we co-opt the Python syntax of assignment to pass an
argument instead.
'''

class TalkativeInt(int):
    def __lshift__(self, other):
        print("Shift", self, "by", other)
        return int.__lshift__(self, other)

t = TalkativeInt(8)
t << 3
#Shift 8 by 3
#64


'STATIC METHODS OF INSTANCES'

'''
One use of classes and their methods that is more closely aligned
with a functional style of programming is to use them simply as
namespaces to hold a variety of related functions.
'''

import math
class RightTriangle(object):
    "Class used solely as namespace for related functions"
    @staticmethod
    def hypotenuse(a, b):
        return math.sqrt(a**2 + b**2)

    @staticmethod
    def sin(a, b):
        return a / RightTriangle.hypotenuse(a, b)

    @staticmethod
    def cos(a, b):
        return b / RightTriangle.hypotenuse(a, b)

RightTriangle.hypotenuse(3,4)
#5.0
rt = RightTriangle()
rt.sin(3,4)
#0.6
rt.cos(3,4)
#0.8

'''
In Python 3.x,
you can pull out functions that have not been so decorated too—i.e.,
the concept of an “unbound method” is no longer needed in
modern Python versions.
'''

import functools, operator
class Math(object):
    def product(*nums):
        return functools.reduce(operator.mul, nums)
    def power_chain(*nums):
        return functools.reduce(operator.pow, nums)

Math.product(3,4,5)
#60

Math.power_chain(3,4,5)
#3486784401

'''
Without @staticmethod , however, this will not work on the instan‐
ces since they still expect to be passed self.
'''

m = Math()
m.product(3,4,5)
'''
TypeError
Traceback (most recent call last)
<ipython-input-5-e1de62cf88af> in <module>()
----> 1 m.product(3,4,5)

<ipython-input-2-535194f57a64> in product(*nums)
      2 class Math(object):
      3   def product(*nums):
----> 4       return functools.reduce(operator.mul, nums)
      5   def power_chain(*nums):
      6       return functools.reduce(operator.pow, nums)

TypeError: unsupported operand type(s) for *: 'Math' and 'int'
'''

'GENERATOR FUNCTIONS'

'''
A special sort of function in Python is one that contains a yield
statement, which turns it into a generator. What is returned from
calling such a function is not a regular value, but rather an iterator
that produces a sequence of values as you call the next() function
on it or loop over it.
'''

'''
Generator functions typically have a great deal of inter‐
nal state; it is at the boundaries of call signature and return value
that they act like a side-effect-free “black box.”
'''

def get_primes():
    "Simple lazy Sieve of Eratosthenes"
    candidate = 2
    found = []
    while True:
        if all(candidate % prime != 0 for prime in found):
            yield candidate
            found.append(candidate)
        candidate += 1

primes = get_primes()
next(primes), next(primes), next(primes)
#(2, 3, 5)
for _, prime in zip(range(10), primes):
    print(prime, end=" ")
#7 11 13 17 19 23 29 31 37 41


'MULTIPLE DISPATCH'

'''
A technique called “multiple dispatch” or sometimes “mul‐
timethods,” the idea here is to declare multiple signatures for a sin‐
gle function and call the actual computation that matches the types
or properties of the calling arguments. This technique often allows
one to avoid or reduce the use of explicitly conditional branching,
and instead substitute the use of more intuitive pattern descriptions
of arguments.
'''

'''
To explain how multiple dispatch can make more readable and less
bug-prone code, let us implement the game of rock/paper/scissors in
three styles.
'''

class Thing(object): pass
class Rock(Thing): pass
class Paper(Thing): pass
class Scissors(Thing): pass

'MANY BRANCHES'

'''
First a purely imperative version.
'''

def beats(x, y):
    if isinstance(x, Rock):
        if isinstance(y, Rock):
            return None  # No winner
        elif isinstance(y, Paper):
            return y
        elif isinstance(y, Scissors):
            return x
        else:
            raise TypeError("Unknown second thing")
    elif isinstance(x, Paper):
        if isinstance(y, Rock):
            return x
        elif isinstance(y, Paper):
            return None # No winner
        elif isinstance(y, Scissors):
            return y
        else:
            raise TypeError("Unknown second thing")
    elif isinstance(x, Scissors):
        if isinstance(y, Rock):
            return y
        elif isinstance(y, Paper):
            return x
        elif isinstance(y, Scissors):
            return None # No winner
        else:
            raise TypeError("Unknown second thing")
    else:
        raise TypeError("Unknown first thing")

rock, paper, scissors = Rock(), Paper(), Scissors()

#>>> beats(paper, rock)
# <__main__.Paper at 0x103b96b00>
# >>> beats(paper, 3)
# TypeError: Unknown second thing

'DELEGATING TO THE OBJECT'

'''
As a second try we might try to eliminate some of the fragile repiti‐
tion with Python’s “duck typing”—that is, maybe we can have differ‐
ent things share a common method that is called as needed.
'''

class DuckRock(Rock):
    def beats(self, other):
        if isinstance(other, Rock):
            return None # No winner
        elif isinstance(other, Paper):
            return other
        elif isinstance(other, Scissors):
            return self
        else:
            raise TypeError("Unknown second thing")

class DuckPaper(Paper):
    def beats(self, other):
        if isinstance(other, Rock):
            return self
        elif isinstance(other, Paper):
            return None # No winner
        elif isinstance(other, Scissors):
            return other
        else:
            raise TypeError("Unknown second thing")

class DuckScissors(Scissors):
    def beats(self, other):
        if isinstance(other, Rock):
            return other
        elif isinstance(other, Paper):
            return self
        elif isinstance(other, Scissors):
            return None # No winner
        else:
            raise TypeError("Unknown second thing")

def beats2(x, y):
    if hasattr(x, 'beats'):
        return x.beats(y)
    else:
        raise TypeError("Unknown first thing")

rock, paper, scissors = DuckRock(), DuckPaper(), DuckScissors()
#>>> beats2(rock, paper)
# <__main__.DuckPaper at 0x103b894a8>
# >>> beats2(3, rock)
# TypeError: Unknown first thing

'PATTERN MATCHING'

'''
we can express all the logic more directly using multi‐
ple dispatch. This should be more readable, albeit there are still a
number of cases to define.
'''

from multipledispatch import dispatch

@dispatch(Rock, Rock)
def beats3(x, y): return None

@dispatch(Rock, Paper)
def beats3(x, y): return y

@dispatch(Rock, Scissors)
def beats3(x, y): return x

@dispatch(Paper, Rock)
def beats3(x, y): return x

@dispatch(Paper, Paper)
def beats3(x, y): return None

@dispatch(Paper, Scissors)
def beats3(x, y): return x

@dispatch(Scissors, Rock)
def beats3(x, y): return y

@dispatch(Scissors, Paper)
def beats3(x, y): return x

@dispatch(Scissors, Scissors)
def beats3(x, y): return None

@dispatch(object, object)
def beats3(x, y):
    if not isinstance(x, (Rock, Paper, Scissors)):
        raise TypeError("Unknown first thing")
    else:
        raise TypeError("Unknown second thing")

#>>> beats3(rock, paper)
#<__main__.DuckPaper at 0x103b894a8>
#>>> beats3(rock, 3)
#TypeError: Unknown second thing


'PREDICATE-BASED DISPATCH'

'''
A really exotic approach to expressing conditionals as dispatch deci‐
sions is to include predicates directly within the function signatures.
'''

from predicative_dispatch import predicate

@predicate(lambda x: x < 0, lambda y: True)
def sign(x, y):
    print("x is negative; y is", y)

@predicate(lambda x: x == 0, lambda y: True)
def sign(x, y):
    print("x is zero; y is", y)

@predicate(lambda x: x > 0, lambda y: True)
def sign(x, y):
    print("x is positive; y is", y)


'LAZY EVALUATION'

'''
Use
of the iterator protocol—and Python’s many built-in or standard
library iteratables—accomplish much the same effect as an actual
lazy data structure.
'''

'''
In a language
like Haskell, which is inherently lazily evaluated, we might define a
list of all the prime numbers in a manner like the following
'''

#-- Define a list of ALL the prime numbers
#primes = sieve [2 ..]
#    where sieve (p:xs) = p : sieve [x | x <- xs, (x `rem` p)/=0]

'''
One can replicate this in Python too, it just isn’t in the
inherent syntax of the language and takes more manual construc‐
tion. Given the get_primes() generator function discussed earlier,
we might write our own container to simulate the same thing.
'''

from collections.abc import Sequence

class ExpandingSequence(Sequence):
    def __init__(self, it):
        self.it = it
        self._cache = []
    def __getitem__(self, index):
        while len(self._cache) <= index:
            self._cache.append(next(self.it))
        return self._cache[index]
    def __len__(self):
        return len(self._cache)

'''
This new container can be both lazy and also indexible.
'''

primes = ExpandingSequence(get_primes())
for _, p in zip(range(10), primes):
    print(p, end=" ")
#2 3 5 7 11 13 17 19 23 29
primes[10]
#31
len(primes)
#11
primes[100]
#547
len(primes)
#101


'THE ITERATOR PROTOCOL'

'''
The easiest way to create an iterator—that is to say, a lazy sequence
—in Python is to define a generator function that uses the yield statement
within the body of a function to define the places
where values are produced.
'''

'''
Technically, the easiest way is to use one of the many iterable
objects already produced by built-ins or the standard library rather
than programming a custom one at all.
'''

iter_seq = iter(sequence)
iter(iter_seq) == iter_seq

'''
The above remarks are a bit abstract, so let us look at a few concrete
examples:
'''

lazy = open('06-laziness.md') # iterate over lines of file
'__iter__' in dir(lazy) and '__next__' in dir(lazy)
#True
plus1 = map(lambda x: x+1, range(10))
plus1  # iterate over deferred computations
#<map at 0x103b002b0>
'__iter__' in dir(plus1) and '__next__' in dir(plus1)
#True
def to10():
    for i in range(10):
        yield i
'__iter__' in dir(to10)
#False
'__iter__' in dir(to10()) and '__next__' in dir(to10())
#True
l = [1,2,3]
'__iter__' in dir(l)
#True
'__next__' in dir(l)
#False
li = iter(l)   # iterate over concrete collection
li
#<list_iterator at 0x103b11278>
li == iter(li)
#True

'''
We can also create custom classes that obey the pro‐
tocol; often these will have other behaviors as well,
but most such behaviors necessarily rely on statefulness and side
effects to be meaningful.
'''

from collections.abc import Iterable
    class Fibonacci(Iterable):
        def __init__(self):
            self.a, self.b = 0, 1
            self.total = 0
        def __iter__(self):
            return self
        def __next__(self):
            self.a, self.b = self.b, self.a + self.b
            self.total += self.a
            return self.a
        def running_sum(self):
            return self.total

# >>> fib = Fibonacci()
# >>> fib.running_sum()
# 0
# >>> for _, i in zip(range(10), fib):
# ...     print(i, end=" ")
# ...
# 1 1 2 3 5 8 13 21 34 55
# >>> fib.running_sum()
# 143
# >>> next(fib)
# 89

'MODULE: ITERTOOLS'

'''
The module itertools is a collection of very powerful—and care‐
fully designed—functions for performing iterator algebra.
'''

'''
Rather than the
stateful Fibonacci class to let us keep a running sum, we might sim‐
ply create a single lazy iterator to generate both the current number
and this sum.
'''

def fibonacci():
    a, b = 1, 1
    while True:
        yeild a
        a, b = b, a+b

from itertools import tee, accumulate

s, t = tee(fibonacci())
pairs = zip(t, accumulate(s))
for _, (fib, total) in zip(range(7), pairs):
    print(fib, total)
#1 1
#1 2
#2 4
#3 7
#5 12
#8 20
#13 33


'CHAINING ITERABLES'

'''
The itertools.chain() and itertools.chain.from_iterable()
functions combine multiple iterables. Built-in zip() and iter
tools.zip_longest() also do this, of course, but in manners that
allow incremental advancement through the iterables. A conse‐
quence of this is that while chaining infinite iterables is valid syntac‐
tically and semantically, no actual program will exhaust the earlier
iterable.
'''

from itertools import chain, count
thrice_to_inf = chain(count(), count(), count())

'''
For merely large
iterables—not for infinite ones—chaining can be very useful and
parsimonious
'''

def from_logs(fnames):
    yield from (open(file) for file in fnames)
lines = chain.from_iterable(from_logs(
              ['huge.log', 'gigantic.log']))


'HIGH ORDER FUNCTIONS'

'''
A higher-order function is simply a function that takes one or
more functions as arguments and/or produces a function as a result.
Many interesting abstractions are available here. They allow chain‐
ing and combining higher-order functions in a manner analogous to
how we can combine functions in itertools to produce new itera‐
bles.
'''

'''
The built-in functions map() and filter() are equivalent to com‐
prehensions—especially now that generator comprehensions are
available—and most Python programmers find the comprehension
versions more readable. For example, here are some (almost) equiv‐
alent pairs:
'''

# Classic "FP-style"
transformed = map(tranformation, iterator)
# Comprehension
transformed = (transformation(x) for x in iterator)
# Classic "FP-style"
filtered = filter(predicate, iterator)
# Comprehension
filtered = (x for x in iterator if predicate(x))

'''
The function functools.reduce() is very general, very powerful,
and very subtle to use to its full power. It takes successive items of an
iterable, and combines them in some way. The most common use
case for reduce() is probably covered by the built-in sum() , which
is a more compact spelling of.
'''

from functools import reduce
total = reduce(operator.add, it, 0)
# total = sum(it)

'''
map() and filter() are also a
special cases of reduce()
'''

add5 = lambda n: n+5
reduce(lambda l, x: l+[add5(x)], range(10), [])
#[5, 6, 7, 8, 9, 10, 11, 12, 13, 14]

# simpler: map(add5, range(10))
isOdd = lambda n: n%2
reduce(lambda l, x: l+[x] if isOdd(x) else l, range(10),[])
#[1, 3, 5, 7, 9]

# simpler: filter(isOdd, range(10))


'UTILITY HIGHER-ORDER FUNCTIONS'

'''
A handy utility is compose() . This is a function that takes a sequence
of functions and returns a function that represents the application of
each of these argument functions to a data argument.
'''

def compose(*funcs):
    """Return a new function s.t.
        compose(f,g,...)(x) == f(g(...(x)))"""
    def inner(data, funcs=funcs):
        result = data
        for f in reversed(funcs):
            result = f(result)
        return result
    return inner

# >>> times2 = lambda x: x*2
# >>> minus3 = lambda x: x-3
# >>> mod6 = lambda x: x%6
# >>> mod6 = lambda x: x%6
# >>> all(f(i)==((i-3)*2)%6 for i in range(1000000))
# True

'''
The built-in functions all() and any() are useful for asking
whether a predicate holds of elements of an iterable. But it is also
nice to be able to ask whether any/all of a collection of predicates
hold for a particular data item in a composable way.
'''

all_pred = lambda item, *tests: all(p(item) for p in tests)
any_pred = lambda item, *tests: any(p(item) for p in tests)

is_lt100 = partial(operator.ge, 100) # less than 100?
is_gt10 = partial(operator.le, 10)   # greater than 10?
from nums import is_prime            # implemented elsewhere

all_pred(71, is_lt100, is_gt10, is_prime)
#True
predicates = (is_lt100, is_gt10, is_prime)
all_pred(107, *predicates)
#False

'''
The library toolz has what might be a more general version of this
called juxt() that creates a function that calls several functions with
Utility Higher-Order Functions
the same arguments and returns a tuple of results.
'''

from toolz.functoolz import juxt

juxt([is_lt100, is_gt10, is_prime])(71)
#(True, True, True)
all(juxt([is_lt100, is_gt10, is_prime])(71))
#True
juxt([is_lt100, is_gt10, is_prime])(107)
#(False, True, True)


'THE OPERATOR MODULE'

'''
For places where you
want to be able to pass a function performing the equivalent of some
syntactic operation to some higher-order function, using the name
from operator is faster and looks nicer than a corresponding
lambda.
'''

# Compare ad hoc lambda with `operator` function
sum1 = reduce(lambda a, b: a+b, iterable, 0)
sum2 = reduce(operator.add, iterable, 0)
sum3 = sum(iterable)  # The actual Pythonic way


'DECORATORS'

'''
A
decorator is just syntax sugar that takes a function as an argument,
and if it is programmed correctly, returns a new function that is in
some way an enhancement of the original function.
'''

@enhanced
def some_func(*args):
    pass

def other_func(*args):
    pass
other_func = enhanced(other_func)
