# SingletonPattern.py

class Singleton:
    class __OnlyOne:
        def __init__(self, arg):
            self.val = arg
        def __str__(self):
            return repr(self) + self.val
    instance = None
    def __init__(self, arg):
        if not Singleton.instance:
            Singleton.instance = Singleton.__OnlyOne(arg)
        else:
            Singleton.instance.val = arg
    def __getattr__(self, name):
        return getattr(self.instance, name)
    def __setattr__(self, name, value):
        return setattr(self.instance, name, value)

class LazySingleton:
    class __OnlyOne:
        def __init__(self):
            self.valf = self
        def __str__(self):
            return repr(self) + self.val
    instance = None
    def __init__(self):
        if not LazySingleton.instance:
            LazySingleton.instance = LazySingleton.__OnlyOne(self)
        else:
            LazySingleton.instance.val = self
        def __getattr__(self, name):
            return getattr(self.instance, name)
        def __setattr__(self, name, value):
            return setattr(self.instance, name, value)

class LazySingletonFixed:
    class __OnlyOne:
        def __init__(self):
            self.valf = self
        def __str__(self):
            return repr(self) + self.val
    instance = None
    def __init__(self):
        if not LazySingletonFixed.instance:
            LazySingletonFixed.instance = LazySingletonFixed.__OnlyOne(self)
        else:
            LazySingletonFixed.instance.val = self
        def __getattr__(self, name):
            return getattr(self.instance, name)
        def __setattr__(self, name, value):
            return setattr(self.instance, name, value)

def test_Singleton():
    x = Singleton('sausage')
    print('x:', x, x.val)
    y = Singleton('eggs')
    print('x:', x, x.val)
    print('y:', y, y.val)
    assert x.val == y.val
    z = Singleton('spam')
    print('x:', x, x.val)
    print('y:', y, y.val)
    print('z:', z, z.val)
    assert x.val == y.val == z.val


if __name__=='__main__':
    test_Singleton()
