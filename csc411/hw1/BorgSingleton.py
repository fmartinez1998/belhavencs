# BorgSingleton.py
# Alex Martelli's 'Borg'

class Borg:
    _shared_state = {}
    def __init__(self):
        self.__dict__ = self._shared_state

class Singleton(Borg):
    def __init__(self, arg):
        Borg.__init__(self)
        self.val = arg
    def __str__(self):
        return '{} : {}'.format(repr(self), self.__dict__)

def test_Borg():
    x = Singleton('sausage')
    print('x:', x, x.val)
    y = Singleton('eggs')
    print('x:', x, x.val)
    print('y:', y, y.val)
    z = Singleton('spam')
    print('x:', x, x.val)
    print('y:', y, y.val)
    print('z:', z, z.val)

if __name__=='__main__':
    test_Borg()
