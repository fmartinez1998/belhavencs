# Francisco Martinez
## CyberSecurity csc404

###ISC Stormcast Thursday November 9, 2017
1.)Gaming keyboard software can exfiltrate data for spellcheck or autocomplete
2.)Easily abused, especially if company loses control or malicious keyboard. 
-Mantistech found their keyboard’s driver exfiltrating data. 
3.)Logitech harmony link, check spam. Logitech has decided to not renew licensure. 
-They will roll out firmware and will no longer support the device
4.)Amazon exposed s3 buckets are recurring issues. 
-New security feature, simple warning for publicly accessible s3 bucket.

###ISC Stormcast Friday November 10, 2017
1.)Certain apps like to use Twilio for sending messages or connecting for calls. All users share the same key data.
2.)Drive by Crypto Currency: software blocked 8 million attempts to download coin hive content. Not terribly impressive. Top 3 million, 2500 actually loading code.
3.)Intel’s management engine
-Different features of CPU/Peripherals
-Maxim found way to turn it off
4.)Gained access through USB 
-Found it’s based on linux
-Impossible to discover and fix. DON’T connect to UNTRUSTED DEVICES
###ISC Stormcast Monday November 13, 2017
1.)Powershell, using cert: leads to certificate store and enumerate all root certificates
2.)Attacker could use these to sign additional certificates that the operating system could trust.
3.)3 ways google accounts are hijacked:
-Re-using passwords, phishing, keylogging
-#1 way is phishing
-safe to use two-factor authentication, and if a user is logging in from an unknown place, 
-then google will block the user.
-A research team hacked the avionics of a Boeing 757. 
-There aren’t many details on it, other than they may have used radio rather than typical wifi or other.

###ISC Stormcast Tuesday November 14, 2017
1.)The face ID feature on the iPhone X can be fooled with a mask. 
-A Vietnamese company used about $150 worth of materials to create a 3d model of a user’s face
-and touched up the most recognizable parts.
2.Heart rate could be used as a way to ID users, according the university of Buffalo. 
-This uses Radar rather than wifi, and has no direct contact with the user. 
-Keeps user logged in, although it is only a prototype.
	

###ISC Stormcast Wednesday November 15, 2017
1.)Microsoft has released updates that address 54 vulnerabilities, though none have been exploited. Office had 6 vulnerabilites fixed as well, with the equation editor function getting an update that was long overdue.
2.)Adobe has also update many of their products across the board, such as flash, shockwave, acrobat, and many others.
3.)Anti-virus software can run as admin, and a user can trick the AV software to restore malicious content in a unaccessible file and cause damage.


###Episode 204

##6 ways awareness programs are failing:
1.Massive competition from other communications programs
2.Learning fatigue
-Many employees end up going through video upon video of training that having even more would just add to the burden of getting them done.
3.	Disinterest
a.	People who do not truly care are not interested, and security professionals think it is insulting
4.	Digital Learning platforms only partially effective
5.	Resources not good communicators
6.	Rely too much on people doing the right thing
7.)There’s the faulty thinking of “security is everyone’s responsibility”, which kind of backfires. The best way to get that same idea across would be to say “Protecting the company is everyone’s responsibility.”
-A researcher came out and said they stumbled upon a similar situation with Equifax last year, 
-though the story is rather damning if it is indeed true and proves a cultural problem. 
-The only issue is the validity of this story, as there is no factual evidence to support this claim. 
-Even so, Equifax incident still brings questions like “Where are we at? How likely will it happen to us?”
-59% of employees paid out of pocket for companies hit with ransomware. 
-An average of $1400 was paid. 
-It’s gotten to where some employers fire employees for falling for phishing scams, 
-even though they have the faulty thinking that employees couldn’t possibly fall for such scams.
-Employees need to be incentivized, but not rewarded constantly.
