#Francisco Martinez
##CSC404 CyberSecurity



#ISC StormCast for Thursday, October 5, 2017

Locky ransomware script has not changed, attachment is a RAR file that unpacks, initialize JavaScript, then spreads malware, current fix(don't allow/open compressed Java attachments)
Viacom s3 bucket leaks, viacom, known owner for MTV, Nickledeon, etc.,had passwords, location, and other access credentials
iOS 11 Outlook bug vulnerability with Apples Wifi security drivers, iOS mail and Office 365 clients cannot sync mail accounts. currently the exact detail of the issue is unknown, but exchange sync is the main issue
Equifax confusion and bad communication with clean up, some publication references other domain the equifaxsecurity2017.com website is not owned by Equifax, doesn't appear harmful.



#ISC StormCast for Friday, September 6, 2017

1.)More DDOS Extortion attempts
-emails claiming DDOS hacks where recipients pay bitcoins for information (Ransomware)
-believed to be run by Phantom Squad, the crew notorious for hacking XBOX and Steam, some
disagree
2.)Microsoft vulnerability used in Cyber Crime Attacks
-the vulnerability is the notorious CVE-2017-8759, our favorite RTF/.net exploit
-sent in the form of email attachment
-threat was an email from the Argentina Tax Agency and had a RTF document disguised as a
word document
3.)CCleaner malware
-Cisco released update on state of CCleaner malware
-some are saying that the second stage of the attack was not effective, but others say different
-attack may have been more targeted than initially thought
4.)Vulnerability in Intel Management Engine
-issue in remote control
-normally only signed code is executed
-hackers can exploit a vulnerability to execute unsigned code
-more details expected in December
-disable feature in Intel CPU



#ISC StormCast for Monday, October 9, 2017

1.(Forensic use of amount...)
-checking for hashes on a filesystem, the big problem is access time
-Mac robber collects hashes but by doing so, updates the access time
-don't want that because it makes you more vulnerable
2.)Adobe accidentally releases private PGP key by mistake
-PGP is good for privacy, provides encryption for email exchange and uploads
-stand alone application for multiple systems
-many companies have public keys they publish, Adobe released private key by mistake and was
exploited
3.)AVAST publishes CCleaner update
-the server used to control the bots is now known
-only 4 days of data logs was available
-reason: too much download traffic flooded the disk
-whoever launched the attack was not truly ready for the volume of data
4.)Android Keyboard App compromised
-mobile keyboard apps have had a spotty privacy record
-they record keystrokes and sends to company's main databases
-reason: to learn typing patterns and language type, aka autocorrect and predictions
-some apps even back data to the cloud¦ YIKES!
-Go Keyboard, an Android keyboard app, operated on the promise no to record data



#ISC StormCast for Tuesday, October 10, 2017

1.)MacOS High Sierra Update
-the security update affected open source components of the OS
-usually patched later which leaves room for exploitation
-nothing super critical at the moment
2.)Possible MacOS keychain leak
-keychain: built in iCloud password safe¦ remember this username and password
-1 year ago, similar exploit, allowed apple script to acknowledge
-new vulnerability exploit without user interaction, user just has to launch a program
3.)Cryptocoin miner found on Showtime Website
-typically placed on websites maliciously
-somehow the JavaScript code was uploaded
-users visiting site were mining cryptocoins without knowing



#ISC StormCast for Wednesday, October 11, 2017

1.)XPCTRA steals Banking/Crypto currency info
-goes for bank account and cryptocurrency info
-arrives as .pdf document claiming to be an invoice
-once installed, installs fiddler http proxy as trusted, used to steal credentials
-fix: certificate pinning
2.)Mobile Investment Applications Vulnerable
-not all secure, less secure than mobile banking apps
-some apps keep certain data unencrypted, can lead to exploits
-don't store passwords on your phone
-TLS connections not validated, man in middle attack
3.)iOS WIFI Exploit POC code published
-Proof of Concept (POC) for vulnerability released by Microsoft
-work against iPhone 7 running old version of iOS 10
-CVE-2017-11120 official name
4.)Android Malware Exploiting Dirty Cow
-announced November 2016
-first time showing up in the wild
-user is tricked to install application, once run, escalates privileges and installs backdoors for
later exploits
