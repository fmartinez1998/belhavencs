# Francisco Martinez
## CyberSecurity csc404

#ISC StormCast for Thursday, November 2,  2017
1.)	SSH configuration, Ethereum, copy/paste
-To properly configure ssh from Rob, copy/paste what he has to offer, 
-https://isc.sans.edu/forums/diary/Securing+SSH+Services+Go+Blue+Team/22992/
-And reconfigure default credentials

2.)Ethereum mining equipment was Hijacked and a reported $600 was lost
-Attacker used default credentials and changing wallet address to attackers wallet
-Crypto-Shoveler watches users clipboard

3.)Google Calendar
-Black hills infosec added gmail user to calendar event by silently adding event to calendar if user hasn’t eliminated auto-add.
-Can be abused with phishing.
-Added to mail sniper tool for emails

#ISC StormCast for Friday, November 3,  2017

1.)Leaked code-signing keys
-325 signed pieces of malware, with 189 properly signed. 
-The problem is that AntiMalware won’t verify the signature for validity and won’t scan a sample either.
2.)Apple had stated in 2015 that it would require all apps published in 2017 to use HTTPS for data transmissions,
-yet still haven’t enforced any of these yet.
-½ of the free apps still don’t secure user credentials adequately.
3.)Chrome Image Downloader basically acts as adware. ID makes it easier to download images, seems unnecessary.

#ISC StormCast for Monday, November 6,  2017
1.)There is a PDF parser that looks through and extracts urls and helps determine wether the content of the file is malicious or not.
2.)OpenSSL patch 
-Works with private keys vulnerability
-Provides less resources for brute force
-Only affects recent processors
3.)IEEE P1735 Encryption Standard is applied to systems that are usually on a chip, such as smartphones. The standard though is broken, and puts the content and integrity of the system at risk.

#ISC StormCast for Tuesday, November 7,  2017
1.)WhatsApp is victim to another problem with a fake version of their app on the Google Play store,
- acting as spamware. It’s received over 1 million downloads
- and features a Unicode space that makes it difficult to distinguish.
2.)Crunchyroll, which is a popular site for watching anime, had an issue with a file, crunchyviewer.exe,
- which would infect your system with malware. The Attackers would get into the cloudflare
- and use the .exe to place malicious software
3.)One way to recover encrypted iOS backups from iOS 10 going on to 11 would be to update
-create a new password through that, rather than try to create a new one
- through scratch with a password you don’t know.
4.)Comcast experienced an outage, but it wasn’t security related.

#ISC StormCast for Wednesday, November 8,  2017
1.)RTF malicious document can actually be easy to spot. Looks for recent Word documents and appends a .exe to the file when Word restarts.
-There have been 79 flaws found with Linux USB ports and have gone unpatched which could lead to DoS attacks
2.)Android has updated and fixed vulnerabilities dealing with media framework and other vulnerabilities
-Ethereum has an issue with $280 million dollars accidentally, causing any wallets from July 20th going on.

