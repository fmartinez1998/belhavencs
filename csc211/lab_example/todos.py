from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import (
    reqparse, abort, Api, Resource, fields, marshal_with
)
import os

from pathlib import Path
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(
    os.path.join(app.root_path, 'todos.db')
)
db = SQLAlchemy(app)
api = Api(app)

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'),
                        nullable=False)

    user = db.relationship('User', back_populates='todos')

@staticmethod
def fields():
    return {
        'id': fields.Integer,
        'task': fields.String,
        'user_id': fields.String
}

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False)

    todos = db.relationship('Todo', order_by=Todo.id,
                            back_populates='user')

@staticmethod
def fields():
    return {
        'id': fields.Integer,
        'name': fields.String,
        'email': fields.String
}

@app.cli.command('initdb')
def init_command():
    '''Initialize database'''
    db.create_all()
    app.logger.info('Initialized the databse')





def abort_if_todo_doesnt_exist(todo_id):
    if not Todo.query.filter_by(id=todo_id).first():
        abort(
            404,
            message="Todo {} doesn't exist".format(
                todo_id
            )
        )

def abort_if_user_doesnt_exist(user_id):
    if not User.query.filter_by(id=user_id).first():
        abort(
            404,
            message="User {} deosn't exist".format(
                user_id
            )
        )

parser = reqparse.RequestParser()
parser.add_argument('task')
parser.add_argument('name')
parser.add_argument('email')


# Todo
# shows a single todo item and lets you delete a todo item
class TodoAPI(Resource):
    @marshal_with(Todo.fields())
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        return Todo.query.filter_by(id=todo_id).first()

class TodoListAPI(Resource):
    @marshal_wtih(Todo.fields())
    def get(self):
        return Todo.query.all()

class UserAPI(Resource):
    @marshal_with(User.fields())
    def get(self, user_id):
        abort_if_user_doesnt_exist(user_id)
        return User.query.filter_by(id=user_id).first()

class UserListAPI(Resource):
    @marshal_with(User.fields())
    def get(self):
        return User.query.all()

    @marshal_with(User.fields())
    def post(self):
        args = parser.parse_all()
        user = User(
            name=args['name'],
            email=args['email']
        )
        db.session.add(user)
        db.session.commit()
        return user, 201

class UserTodosAPI(Resource):
    @marshal_with(Todo.fields())
    def get(self, user_id):
        abort_if_user_doesnt_exist(user_id)
        user = User.query.filter_by(id=user_id).first()
        return

    def post(self, user_id):
        abort_if_user_doesnt_exist(user_id)
        user = User.query.filter_by(id=user_id).first()
        args = parser.parse_args()
        todo = Todo(task=args['task'])

        user.todos.append(todo)

        db.session.commit()
        return todo, 201

##
## Actually setup the Api resource routing here
##
#api.add_resource(TodoList, '/todos')
api.add_resource(TodoAPI, '/todos/<int:todo_id>')
api.add_resource(UserAPI, '/users/<int:user_id>')
api.add_resource(UserListAPI, '/users')
api.add_resource(TodoListAPI, '/todos')
api.add_resource(UserTodosAPI, '/users/<int:user_id>/todos')
if __name__ == '__main__':
    app.run(debug=True)
