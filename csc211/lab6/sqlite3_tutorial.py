import sqlite3
import hashlib



def encrypt_password(password):
    encrypted_passw = hashlib.sha256(password.encode('utf-8')).hexdigest()
    return encrypted_passw

cursor = sqlite3.connect("users.sqlite",detect_types=sqlite3.PARSE_DECLTYPES)

db = sqlite3.connect(':memory:')
db.create_function("encrypt", 1, encrypt_password)

db = sqlite3.connect('users.db')

cursor = db.cursor()

cursor.execute('''
  CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT,
                   phone TEXT, email TEXT unique, password TEXT, created_at Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)''');

users = open('lab6-names.csv', 'r')
script = users.read()
#cursor.executescript(script)
db.commit()
users.db = script
# Print the results
cursor.execute('''SELECT * FROM users where phone like"9%" ''')
for phone in cursor:
    print(phone)
db.close()
