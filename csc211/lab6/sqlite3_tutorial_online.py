import sqlite3

#<!-- from importlib import reload
#reload(tutorial) -->


#create a database in RAM
db = sqlite3.connect(':memory:')
#creates or opens a file called mydb with a SQLite3 DB
db = sqlite3.connect('data/mydb')

db.close()

# Get a cursor object
cursor = db.cursor()
cursor.execute('''
    CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT,
                        phone TEXT, email TEXT unique, password TEXT)
''')
db.commit()

# GET a cursor object
cursor = db.cursor()
cursor.execute('''DROP TABLE users''')
db.commit()

cursor = db.cursor()
name1 = 'Andres'
phone1 = '3366858'
email1 = 'user@example.com'
# A Very Secure password
password1 = '122345'
