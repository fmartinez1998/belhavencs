import bz2
import io
import csv
from pathlib import Path

import requests

def get_verses():
    kjv_path = Path('kjv.tsv.bz2')
    if not kjv_path.exists():
        data = requests.get(
            'https://belhavencs.nyc3.digitaloceanspaces.com/csc211/kjv.tsv.bz2'
        ).content
        kjv_path.write_bytes(data)
        kjv_data = io.BytesIO(data)
    else:
        kjv_data = io.BytesIO(kjv_path.read_bytes())

    with bz2.open(kjv_data, 'rt') as rfp:
        columns = ['book','chapter','verse','text']
        for row in csv.DictReader(rfp, columns, delimiter='\t'):
            yield row
