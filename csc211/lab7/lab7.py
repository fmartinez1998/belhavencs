from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String
import csv
import sqlite3
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite3:///bible.db'
db = SQLAlchemy(app)
engine = create_engine('sqlite:///:memory:', echo=True)
session = sessionmaker(bind=engine)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
Base = declarative_base()

#verses
class Verse(db.Model):
    __tablename__ = 'verses'

    id = db.Column(db.Integer, primary_key=True)
    book = db.Column(db.String, unique=True, nullable=False)
    chapter = db.Column(db.Integer, unique=True, nullable=False)
    verse = db.Column(db.Integer, unique=True, nullable=False)
    text = db.Column(db.String, unique=True, nullable=False)

    comments = db.relationship('Comment', back_populates='Verse')
    def __repr__(self):
        return "<Verse(id='%s', book='%s', chapter='%s', verse='%s', text='%s')>" % (
                            self.id, self.book, self.chapter, self.verse, self.text
    )

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    phone = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False)
    password = db.Column(db.String)

    comments = db.relationship('Comment', back_populates='User')
    def __repr__(self):
        return "<User(id='%s', name='%s', phone='%s', email='%s', password='%s')>" % (
                            self.id, self.name, self.phone, self.email, self.password
    )

class Comment(db.Model):
    __tablename__ = 'comments'

    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users'))
    verse_id = db.Column(db.Integer, db.ForeignKey('verses'))

    user = db.relationship('User', back_populates='comments')
    verse = db.relationship('Verse', back_populates='comments')


Base.metadata.create_all(engine)



def read_in_users():
    user_data = []
    with open('lab7-names.csv') as rfp:
        reader = csv.reader(rfp)
        columns = next(reader)
        dict_reader = csv.DictReader(rfp, columns)
        for row in dict_reader:
            user_data.append(row)
    return user_data


INSERT_USERS = '''
INSERT INTO users(name, phone, email, password)
    VALUES(:name, :phone, :email, :password)
'''
def insert_users(users):
    with get_db() as db:
        cursor =db.cursor()
        cursor.executemany(INSERT_USERS, users)
        db.commit()
    return True


def main():
    pass
